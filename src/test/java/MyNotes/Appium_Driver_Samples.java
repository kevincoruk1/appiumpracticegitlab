package MyNotes;


import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Appium_Driver_Samples
{
    public static AppiumDriverLocalService service;
    public static DesiredCapabilities deCaps = null;
    public static IOSDriver<MobileElement> appiumDriver;

    public static void startAppiumDriver()
    {
//        // SRMS
//        deCaps = new DesiredCapabilities();
//        deCaps.setCapability(MobileCapabilityType.PLATFORM_NAME, getPropertyByKey("platformName"));
//        deCaps.setCapability(MobileCapabilityType.PLATFORM_VERSION, getPropertyByKey("platformVersion"));
//        deCaps.setCapability(MobileCapabilityType.DEVICE_NAME, getPropertyByKey("deviceName"));
//        deCaps.setCapability(MobileCapabilityType.AUTOMATION_NAME, getPropertyByKey("automationName"));
//        deCaps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, getPropertyByKey("newCommandTimeOut"));
//        deCaps.setCapability(MobileCapabilityType.APP, System.getProperty("user.dir") + getPropertyByKey("application"));
//        deCaps.setCapability(MobileCapabilityType.APP, getPropertyByKey("bundleID"));
//        deCaps.setCapability(MobileCapabilityType.FULL_RESET, getPropertyByKey("fullResetFalse"));
//        deCaps.setCapability(MobileCapabilityType.NO_RESET, getPropertyByKey("noResetTrue"));
//        deCaps.setCapability("usePrebuiltWDA", true);
//        //deCaps.setCapability(MobileCapabilityType.UDID, getPropertyByKey("simulator_UDID"));

//          deCaps.setCapability(MobileCapabilityType.UDID, getPropertiesByKey("deviceUDID"));  //real device
//          deCaps.setCapability("xcodeOrgId", "your team id");                //real device
//          deCaps.setCapability("xcodeSigningId", "iPhone Developer");      //real device
//          deCaps.setCapability("updateWDABundleId", "app bundle id");    //real device


        // ANDROID
        //ApiDemos-debug.apk
//        deCaps.setCapability(MobileCapabilityType.DEVICE_NAME, getPropertyByKey("deviceName")); //Pixel4_XL_6.3
//        deCaps.setCapability(MobileCapabilityType.PLATFORM_NAME, getPropertyByKey("platformName")); //Android
//        deCaps.setCapability("avd", getPropertyByKey("deviceName")); //Pixel4_XL_6.3
//        deCaps.setCapability(MobileCapabilityType.NO_RESET, "true");
//        deCaps.setCapability(MobileCapabilityType.AUTOMATION_NAME, getPropertyByKey("automationName")); //uiautomator2
//        deCaps.setCapability(MobileCapabilityType.APP, System.getProperty("user.dir") + getPropertyByKey("application")); ///src/test/java/AndroidApiDemosDebug/Resources/ApiDemos-debug.apk


        // To execute the browser on an Android device
        // ITS GIVING NULLPOINTEREXCEPTION error need to look at later
//
//            deCaps.setCapability(MobileCapabilityType.PLATFORM_NAME, "platformName");
//            deCaps.setCapability(MobileCapabilityType.VERSION, "10");
//            deCaps.setCapability(MobileCapabilityType.BROWSER_NAME,"Chrome");
//            deCaps.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Device");
//            deCaps.setCapability("chromedriverUseSystemExecutable", true);

        //Run an app on Android device
//            deCaps.setCapability(MobileCapabilityType.DEVICE_NAME, getPropertyByKey("deviceName"));
//            deCaps.setCapability(MobileCapabilityType.PLATFORM_NAME, getPropertyByKey("platformName"));
//            deCaps.setCapability(MobileCapabilityType.VERSION, getPropertyByKey("AndroidVersion"));
//            deCaps.setCapability("appPackage", "bundle id here");
//            deCaps.setCapability("appActivity","com.vfc.MainActivity");


//      //General-Store.apk
//            deCaps.setCapability("deviceName", "Pixel4_XL_6.3"); //"Pixel4_XL_6.3" //"Android Device"
//            deCaps.setCapability("platformName", "Android");
//            deCaps.setCapability("platformVersion","11");
//            // deCaps.setCapability("version","11");
//            deCaps.setCapability("noReset","true");
//            deCaps.setCapability("automationName", "uiautomator2");
//            deCaps.setCapability("appPackage", "com.androidsample.generalstore");
//            deCaps.setCapability("appActivity","com.androidsample.generalstore.SplashActivity");
//            deCaps.setCapability("app", System.getProperty("user.dir") + "/src/test/java/AndroidApiDemosDebug/Resources/General-Store.apk");
    }
}
