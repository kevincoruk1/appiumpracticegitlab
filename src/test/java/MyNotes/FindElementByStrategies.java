package MyNotes;


import AndroidApiDemosDebug.CommonLibrary.Android_Driver;
import org.testng.annotations.Test;


public class FindElementByStrategies
{


    public static void main(String[] args)
    {

        findElement();

    }
    @Test(priority = 1)
    public static void findElement()
    {
        //*****************************************************************************************************************************
        //ANDROID
/*
        Index:	2
        iOS_Driver.appiumDriver.findElementsById("android:id/text1").get(2).click();
        iOS_Driver.appiumDriver.findElementByAndroidUIAutomator("index(2)").click();

        text:	App
        iOS_Driver.appiumDriver.findElementByAndroidUIAutomator("text(\"App\")").click();

        resource-id:	android:id/text1
        iOS_Driver.appiumDriver.findElementById("android:id/text1").click();
        iOS_Driver.appiumDriver.findElementsById("android:id/text1").get(2).click();

        class:	android.widget.TextView
        iOS_Driver.appiumDriver.findElementsByClassName("android.widget.TextView").get(2).click();
        iOS_Driver.appiumDriver.findElementsByAndroidUIAutomator("new UiSelector().className(\"android.widget.TextView\")").size();

        package:	io.appium.android.apis

        content-desc:	App
        iOS_Driver.appiumDriver.findElementByAccessibilityId("App").click();
        iOS_Driver.appiumDriver.findElementsByAndroidUIAutomator("new UiSelector().description(\"NFC\")").size();

        checkable:	FALSE
        iOS_Driver.appiumDriver.findElementsByAndroidUIAutomator("new UiSelector().description(\"NFC\")").size();

        checked:	FALSE
        clickable:	FALSE
        enabled:	TRUE
        focusable:	FALSE
        focused:	FALSE
        scrollable:	FALSE
        long-clickable:	FALSE
        password:	FALSE
        selected:	FALSE
        bounds:	[0,712][1440,880]
 */
        // find all the elements with id and click on the first index
        // pay attention '.findElementsById' is with s
        Android_Driver.appiumDriver.findElementsById("android:id/text1").get(0).click();
        // find element by 'text' attribute
        Android_Driver.appiumDriver.findElementByAndroidUIAutomator("text(\"Animation\")").click();
        // find element by class >>> element has to be plural (findElementsByClassName)
        Android_Driver.appiumDriver.findElementsByClassName("android.widget.TextView").get(2).click();
        // accessibilityid in ios is content-desc attribute in Android
        Android_Driver.appiumDriver.findElementByAccessibilityId("Animation").click();
        //id in ios is resource-id in android
        Android_Driver.appiumDriver.findElementById("android:id/text1").click();

        //XPATH
        // those 2 scripts will click on the same element 'index 4'
        Android_Driver.appiumDriver.findElementByXPath("(//android.widget.TextView[4])").click();
        Android_Driver.appiumDriver.findElementsByXPath("(//android.widget.TextView)").get(4).click();

        Android_Driver.appiumDriver.findElementsByXPath("//android.widget.TextView[@checkable='false']").get(2).click();
        Android_Driver.appiumDriver.findElementsByXPath("//android.widget.TextView[@package='io.appium.android.apis']").get(2).click();
        Android_Driver.appiumDriver.findElementsByXPath("//android.widget.TextView[@class='android.widget.TextView']").get(2).click();
        Android_Driver.appiumDriver.findElementsByXPath("//android.widget.TextView[@resource-id='android:id/text1']").get(2).click();

        Android_Driver.appiumDriver.findElementByXPath("//android.widget.TextView[@index='2']").click();
        Android_Driver.appiumDriver.findElementByXPath("//android.widget.TextView[@text='App']").click();
        Android_Driver.appiumDriver.findElementByXPath("//android.widget.TextView[@content-desc='App']").click();


        //find element using 'new UiSelector()'
        // full list of method >>>https://developer.android.com/reference/androidx/test/uiautomator/UiSelector
        int number1 = Android_Driver.appiumDriver.findElementsByAndroidUIAutomator("new UiSelector().index(9)").size();
        System.out.println("the size for index(\"9\") is " + number1);
        int number2 = Android_Driver.appiumDriver.findElementsByAndroidUIAutomator("new UiSelector().text(\"Views\")").size();
        System.out.println("the size for text(\"Views\") is " + number2);
//        int number3 = appiumDriver.findElementsByAndroidUIAutomator("new UiSelector().id(\"android:id/text1\")").size(); //resource-id not working
//        System.out.println("the size for resource-id(\"android:id/text1\") is " + number3);
        int number4 = Android_Driver.appiumDriver.findElementsByAndroidUIAutomator("new UiSelector().className(\"android.widget.TextView\")").size();
        System.out.println("the size for className(\"android.widget.TextView\") is " + number4);
//        int number5 = appiumDriver.findElementsByAndroidUIAutomator("new UiSelector().package(\"io.appium.android.apis\")").size(); //not working
//        System.out.println("the size for package(\"io.appium.android.apis\") is " + number5);
        int number6 = Android_Driver.appiumDriver.findElementsByAndroidUIAutomator("new UiSelector().description(\"NFC\")").size(); //content-desc
        System.out.println("the size for content-desc(\"NFC\") is " + number6);
        //the syntax for checkable, checked,clickable etc. is different
        int number7 = Android_Driver.appiumDriver.findElementsByAndroidUIAutomator("new UiSelector().checkable(false)").size();
        System.out.println("the size for \"new UiSelector().checkable(false)\" is " + number7);





//*****************************************************************************************************************************








        //SRMS
//
//        appiumDriver.findElementByAccessibilityId("Choose a Category").click();
//        appiumDriver.findElementById("Accessories").click();
//        //myDriver.findElementByClassName("XCUIElementTypeStaticText").click();   // need an element here
//        appiumDriver.findElementByName("Choose a Style").click(); // choose a color
//        // full xpath (xpath for the cell)
//        appiumDriver.findElementByXPath("//XCUIElementTypeApplication[@name=\"VF SRMS\"]/XCUIElementTypeWindow[1]" +
//                "/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther" +
//                "/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]").click(); // Style: A SKATE BAG
//        //by cell index
//        appiumDriver.findElementByXPath("//XCUIElementTypeCell[3]").click(); // choose a color
//        // by name
//        appiumDriver.findElementByXPath("//XCUIElementTypeStaticText[@name=\"Black/White\"]").click(); // color: BLACK/WHITE
//        // name starts with
//        appiumDriver.findElementByXPath("//XCUIElementTypeButton[starts-with(@name,'SEAR')]").click();  // Search button
//        // ends with
//        appiumDriver.findElementByXPath("//XCUIElementTypeButton[ends-with(@name,'xe')]").click();  // style
//        // name contains
//        appiumDriver.findElementByXPath("//XCUIElementTypeButton[contains(@name,'Men')]").click(); // size_type
//        // multiple values (and)
//        appiumDriver.findElementByXPath("//XCUIElementTypeButton[contains(@name,'PICK') and @label = 'PICK']").click(); // pick tab
//        // multiple values (or)
//        appiumDriver.findElementByXPath("//XCUIElementTypeButton[contains(@name,'PICK') or @label = 'Pick List']").click(); // pick tab
//
//        appiumDriver.findElementByIosNsPredicate("name BEGINSWITH 'STYL'");
//        appiumDriver.findElementByIosNsPredicate("name ENDSWITH 'LE'");
//        appiumDriver.findElementByIosNsPredicate("label BEGINSWITH 'STYL'");
//        appiumDriver.findElementByIosNsPredicate("label ENDSWITH 'LE'");
//
//      //  myDriver.findElementByXPath("//XCUIElementTypeStaticText[@name=\"LOCATION\"])[1]//following-sibling::XCUIElementTypeStaticText[1").click(); // need an element here
//
//        appiumDriver.findElementByXPath("//XCUIElementTypeCell[1]/XCUIElementTypeStaticText[11]").click(); // device name
//        appiumDriver.findElementByXPath("//XCUIElementTypeCell[1]/XCUIElementTypeStaticText[4]").click(); // style value
//        appiumDriver.findElementByXPath("//XCUIElementTypeCell[1]/XCUIElementTypeStaticText[5]").click(); // color value
//        appiumDriver.findElementByXPath("//XCUIElementTypeCell[1]/XCUIElementTypeStaticText[6]").click(); // quantity value
//        appiumDriver.findElementByXPath("//XCUIElementTypeCell[1]/XCUIElementTypeStaticText[7]").click(); // location value
//        appiumDriver.findElementByXPath("//XCUIElementTypeCell[1]/XCUIElementTypeStaticText[8]").click(); // plusSign
//        appiumDriver.findElementByXPath("//XCUIElementTypeCell[1]/XCUIElementTypeStaticText[10]").click(); // size value
//
//        appiumDriver.findElementByXPath("//XCUIElementTypeCell[1]/XCUIElementTypeStaticText[1]").click(); // color label
//        appiumDriver.findElementByXPath("//XCUIElementTypeCell[1]/XCUIElementTypeStaticText[2]").click(); // quantity label
//        appiumDriver.findElementByXPath("//XCUIElementTypeCell[1]/XCUIElementTypeStaticText[3]").click(); // location label
//        appiumDriver.findElementByXPath("//XCUIElementTypeCell[1]/XCUIElementTypeStaticText[9]").click(); // size label
//        appiumDriver.findElementByXPath("//XCUIElementTypeCell[1]/XCUIElementTypeStaticText[12]").click(); // style label





    }
}