package LearningJava.TimBuchalka.Switch;

public class SwitchCase
{


    /*
   when switch is feasible
   1- Readability
   2- Intent ??


when switch is not feasible
   1- More than one condition to test
   2- Test other than equality, e.g., month >3
   3- Switch expression is not integer, string, or enum
   4- A case label restriction does not apply

     */




    public static void main(String[] args)
    {
        int value= 1;
        if (value == 1)
        {
            System.out.println("The value is "+ value);
        } else if (value == 2)
        {
            System.out.println("The value is "+ value);
        } else if (value == 3)
        {
            System.out.println("The value is "+ value);
        }

        int switchValue = 3;
        switch(switchValue)
        {
            case 1:
                System.out.println("This is value 1");
                break;
            case 2:
                System.out.println("This is value 2");
                break;
            case 3:
                System.out.println("This is value 3");
                break;
            default:
                System.out.println("This is the default value");
                break;
        }


        char charValue='B';
        switch(charValue)
        {
            case 'A':
                System.out.println("This is value A");
                break;
            case 'B':
                System.out.println("This is value B");
                break;
            case 'C':
                System.out.println("This is value B");
                break;
            default:
                System.out.println("This is value default");
                break;
        }

        String stringValue= "january";
        switch(stringValue)
        {
            case "january":
                System.out.println("This is value january");
                break;
            case "february":
                System.out.println("This is value february");
                break;
            case "march":
                System.out.println("This is value march");
                break;
            default:
                System.out.println("This is value default");
                break;
        }

        byte month= 0; // case must be within the switch range
        switch(month)
        {
           // case 128 : // this will give us compilling error as its out of byte range
               // System.out.println("This is month january");

        }

        int day= 0; // case must be within the switch range
        switch(month)
        {
            case 1 :
                System.out.println("This is day 1");
            //case 2.0 :  // this will give us compilling error as its double data type
                System.out.println("This is day 2");

        }
    }
}
