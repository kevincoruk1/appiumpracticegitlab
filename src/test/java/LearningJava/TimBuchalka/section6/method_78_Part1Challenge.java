package LearningJava.TimBuchalka.section6;

// this code will be called in "method_78_Part1ChallengeMain" class
public class method_78_Part1Challenge
{
    private String number;
    private double balance;
    private String customerName;
    private String email;
    private String phoneNumber;

    public void deposit(int depositAmount)
    {
        this.balance += depositAmount;
        System.out.println("Deposited "+depositAmount+" the new balance is "+this.balance);
    }
    public void withdrawal(double withdrawalAmount)
    {
        if(withdrawalAmount <= this.balance)
        {
            this.balance -= withdrawalAmount;
            System.out.println("Withdraw "+withdrawalAmount+" the new balance is ="+this.balance);
        } else
        {
            System.out.println("Insufficient fund. Can not withdraw");
        }
    }

    public String getNumber()
    {
        return number;
    }

    public void setNumber(String number)
    {
        this.number = number;
    }

    public double getBalance()
    {
        return balance;
    }

    public void setBalance(double balance)
    {
        this.balance = balance;
    }

    public String getCustomerName()
    {
        return customerName;
    }

    public void setCustomerName(String customerName)
    {
        this.customerName = customerName;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }
}
