package LearningJava.TimBuchalka.section6;

public class method_77_29_SimpleCalculator
{
    private double firstNumber;
    private double secondNumber;


    public void setFirstNumber(double firstNumber)
    {
        this.firstNumber = firstNumber;
    }

    public void setSecondNumber(double secondNumber)
    {
        this.secondNumber = secondNumber;
    }

    public double getFirstNumber()
    {
        return this.firstNumber;
    }

    public double getSecondNumber()
    {
        return this.secondNumber;
    }

    public double getAdditionResult()
    {
        //both of those line working :)
        //return getFirstNumber() + getSecondNumber();
        return this.firstNumber + this.secondNumber;
    }

    public double getSubstractionResult()
    {
        return getFirstNumber() - getSecondNumber();
    }

    public double getMultipicationResult()
    {
        return getFirstNumber() * getSecondNumber();
    }

    public double getDivisionResult()
    {
        if(getSecondNumber() == 0)
        {
            System.out.println("the second number is 0 so division result is 0");
            return 0;
        } else
        {
            return getFirstNumber() / getSecondNumber();
        }
    }
}
