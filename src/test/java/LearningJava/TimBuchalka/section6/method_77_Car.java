package LearningJava.TimBuchalka.section6;

// the method created here are called in "method_77_ClassesPart1andPart2" class
public class method_77_Car
{
    private int doors;
    private int wheels;
    private String model;
    private String engine;
    private String color;

    public void setModel(String model)
    {
      String validModel = model.toLowerCase();
      if (validModel.equals("porsche") || validModel.equals("holden"))
      {
          // this.model is the one that is assigned in the class as a string.
          // and =model is the model string that is the parameter in setModel method.
          this.model = model;
      } else
      {
          this.model = "Unknown";
      }
    }

    public String getModel()
    {
        return this.model;
    }

}
