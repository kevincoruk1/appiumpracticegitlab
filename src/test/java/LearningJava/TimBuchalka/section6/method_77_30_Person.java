package LearningJava.TimBuchalka.section6;

// the method created here are called in "method_77_30_PersonMain" class
public class method_77_30_Person
{

    private String firstName;
    private String lastName;
    private int age;

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }
    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }
    public void setAge(int age)
    {
        if (age < 0 || age > 100)
        {
            System.out.println("The age is not in the range of 0-100");
        } else
        {
            this.age = age;
        }
    }
    public String getFirstName()
    {
        return this.firstName;
    }
    public String getLastName()
    {
        return this.lastName;
    }
    public String getFullName()
    {
        if (firstName == "" && lastName == "")
        {
            //System.out.println("There is no name no lastname");
            return "empty string";
        }else if (firstName =="")
        {
            //System.out.println("The first name is empty returning last name = "+this.lastName);
            return this.lastName;
        }else if (lastName.isEmpty())  //lastName.isEmpty is the same as lastName == ""
        {
            //System.out.println("The last name is empty returning first name = "+this.firstName);
            return this.firstName;
        } else
        {
            //System.out.println("Full name is = "+this.firstName+" "+this.lastName);
            return this.firstName +" "+ this.lastName;
        }
    }
    public int getAge()
    {
        return this.age;
    }
    public boolean isTeen()
    {
        if (age >12 && age <20)
        {
            //System.out.println("yes "+age+", its a teenager ");
            return true;
        }else
        {
            //System.out.println("No its not a teenager");
            return false;
        }
    }
}
