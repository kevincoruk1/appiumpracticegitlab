package LearningJava.TimBuchalka.section5;

import java.util.Scanner;

public class method_72_ProblemAndSolution
{
    public static void main(String[] args)
    {
        //userInput();
        userInputValidate();
    }


    // This method does not validate input, it just print whatever user enter like if you enter -20 for age
    public static void userInput()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter your year of birth: ");
        int yearOfBirth = scanner.nextInt();
        // if you dont write this line it will execute all line without waiting input from user
        scanner.nextLine();

        System.out.println("Enter your name: ");
        String name = scanner.nextLine();

        int age = 2020-yearOfBirth;

        System.out.println("Your name is : "+name+" and you are "+age+" years old.");

        scanner.close();
    }

    // This method validate if user input is correct
    public static void userInputValidate()
    {
        // create a object of scanner
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter your year of birth: ");
        // check if entered value is integer
        boolean hasNextInt = scanner.hasNextInt();

        if (hasNextInt)
        {
            int yearOfBirth = scanner.nextInt();
            scanner.nextLine();

            System.out.println("Enter your name: ");
            String name = scanner.nextLine();

            // calculate the age
            int age = 2020-yearOfBirth;
            if (yearOfBirth >= 0)
            {
                System.out.println("Your name is : "+name+" and you are "+age+" years old.");
            } else
            {
                System.out.println("Value for year can not be less than zero");
            }
        } else
        {
            System.out.println("Value for year should be number only");
        }
        scanner.close();
    }
}

