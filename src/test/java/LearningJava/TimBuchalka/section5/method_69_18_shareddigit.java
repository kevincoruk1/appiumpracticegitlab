package LearningJava.TimBuchalka.section5;

public class method_69_18_shareddigit
{
    public static void main(String[] args)
    {
        hasSharedDigit(14,24);
    }

    // find the shareddigit between 2 numbers

    public static boolean hasSharedDigit(int number1, int number2)
    {
        if (number1 < 10 || number1 >99 || number2 < 10 || number2 >99)
        {
            System.out.println("False");
            return false;
        } else
        {
            int secondDigitOfNumber1 = number1;
            int secondDigitOfNumber2 = number2;
            int firstDigitOfNumber1 = 0;
            int firstDigitOfNumber2 = 0;

            firstDigitOfNumber1 = number1 % 10;
            secondDigitOfNumber1 = number1 / 10;
            firstDigitOfNumber2 = number2 % 10;
            secondDigitOfNumber2 = number2 / 10;
            System.out.println("The first digit of " + number1 + " = " + firstDigitOfNumber1);
            System.out.println("The second digit of " + number1 + " = " + secondDigitOfNumber1);

            System.out.println("The first digit of " + number2 + " = " + firstDigitOfNumber2);
            System.out.println("The second digit of " + number2 + " = " + secondDigitOfNumber2);

            if (firstDigitOfNumber1 == firstDigitOfNumber2 || firstDigitOfNumber1 == secondDigitOfNumber2 ||
                    secondDigitOfNumber1 == firstDigitOfNumber2 || secondDigitOfNumber1 == secondDigitOfNumber2)
            {
                if (firstDigitOfNumber1 == firstDigitOfNumber2)
                {
                    System.out.println(number1 + " and " + number2 + " has a shared digit " + firstDigitOfNumber1);
                    return true;
                } else if (firstDigitOfNumber1 == secondDigitOfNumber2)
                {
                    System.out.println(number1 + " and " + number2 + " has a shared digit " + firstDigitOfNumber1);
                    return true;
                } else if (secondDigitOfNumber1 == firstDigitOfNumber2)
                {
                    System.out.println(number1 + " and " + number2 + " has a shared digit " + secondDigitOfNumber1);
                    return true;
                }else if (secondDigitOfNumber1 == secondDigitOfNumber2)
                {
                    System.out.println(number1 + " and " + number2 + " has a shared digit " + secondDigitOfNumber1);
                    return true;
                }
            }
            System.out.println("There is no shared digit between "+number1+" and "+number2);
            return false;
        }
    }
}

