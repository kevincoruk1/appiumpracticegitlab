package LearningJava.TimBuchalka.section5;

public class method_69_21_allFactors
{
    public static void main(String[] args)
    {
        printFactors(100);
    }

    // print the factors of a number

    public static void printFactors(int number)
    {
        int factors = 0;
        if (number < 1)
        {
            System.out.println("Invalid Value");
        } else
        {
            for (int i = 1; i <= number; i++)
            {
                factors = number % i;
                if (number % i == 0)
                {
                    System.out.print(i+" ");
                }
            }
        }
    }
}

