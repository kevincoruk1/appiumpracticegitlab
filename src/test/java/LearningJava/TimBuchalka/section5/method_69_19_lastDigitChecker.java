package LearningJava.TimBuchalka.section5;

public class method_69_19_lastDigitChecker
{
    public static void main(String[] args)
    {
        hasSameLastDigit (152,200,252);
//        isValid (1);
//        isValid (11);
//        isValid (999);

    }


    // check to see if any of the 3 numbers has the same last digit

     public static boolean hasSameLastDigit (int number1, int number2, int number3)
    {
        int lastDigit1= number1 %10;
        System.out.println("last dgit1 "+ lastDigit1);
        int lastDigit2= number2 %10;
        System.out.println("last dgit2 "+ lastDigit2);
        int lastDigit3= number3 %10;

        if (number1 <10 || number1 >1000 || number2 <10 || number2 >1000 || number3 <10 || number3 >1000 )
        {
            System.out.println("False");
            return false;
        } else
        {
            if (lastDigit1 == lastDigit2 || lastDigit1 == lastDigit3 || lastDigit2 == lastDigit3 )
            {
                System.out.println("True, at least 2 numbers share the last digit");
                return true;
            }
            System.out.println("False, neither of the numbers share the last digit");
            return false;
        }
    }

    public static boolean isValid(int number)
    {
        if (number >= 10 && number <= 1000)
        {
            System.out.println("True, the given number is in the range of 10-1000");
            return true;
        }
        System.out.println("False, the given number is NOT in the range of 10-1000");
        return false;
    }
}

