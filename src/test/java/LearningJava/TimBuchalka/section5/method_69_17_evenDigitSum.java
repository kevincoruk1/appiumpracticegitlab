package LearningJava.TimBuchalka.section5;

public class method_69_17_evenDigitSum
{
    public static void main(String[] args)
    {
        getEvenDigitSum(22);
    }

    /// add up the even digit in a given number

    public static int getEvenDigitSum(int number)
    {
        int wholeNumber = number;
        int remainingNumber = 0;
        int sum = 0;


      if (number <0)
      {
          System.out.println("-1");
          return -1;
      } else
      {
          while (wholeNumber >0)
          {
              {
                  remainingNumber = wholeNumber % 10;
                  System.out.println("the remaining number = " + remainingNumber);

                  wholeNumber = wholeNumber / 10;
                  System.out.println("the whole number = " + wholeNumber);

                  System.out.println("******************");
              }

              if (remainingNumber % 2 ==0)
              {
                  sum = sum +remainingNumber;
                  System.out.println("the sum = " + sum);
              }

          }
          System.out.println("The sum of even digits = " + sum);
          return sum;
      }
    }
}

