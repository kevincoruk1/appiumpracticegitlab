package LearningJava.TimBuchalka.section5;

public class method_69_22_perfectNumber
{
    public static void main(String[] args)
    {
        //isPerfectNumber(5);
        iisPerfectNumber(28);
    }

    //find the perfect number
    // that is sum of its divisors are = the number itself
    // like 6.
    public static boolean isPerfectNumber(int number)
    {
        int devisors = 0;
        int sumDevisors = 0;
        int remaining = 0;
        if (number < 1)
        {
            System.out.println("False");
            return false;

        } else
        {
            for (int i = 1; i <= number; i++)
            {
                remaining = number % i;
                if (remaining == 0)
                {
                    System.out.println("This is on devisor: " + i);
                    devisors = i;
                    if (devisors != number)
                    {
                        sumDevisors = sumDevisors + i;
                    }
                }
            }
            System.out.println("The sum of devisors is = " + sumDevisors);
            if (sumDevisors == number)
            {
                System.out.println("The number is "+number+ " and the sum of devisors is = "+sumDevisors+" So, this is a perfect number ");
                return true;
            }
            System.out.println("The number is "+number+ " and the sum of devisors is = "+sumDevisors+" So, this is NOT a perfect number ");
            return false;
        }
    }

    // solving the same issue with different way
    public static boolean iisPerfectNumber(int number)
    {

        if(number<1) return false;

        int sum=0;

        for(int i=1;i<number;i++)
        {
            if(number%i==0)
            {
                System.out.println("This is i value: " + i);
                sum += i;
                System.out.println("This is sum value: " + sum);
            }
        }
        System.out.println("This is sum value2: " + sum);
        System.out.println("This is number value2: " + number);
        return sum == number;
    }
}

