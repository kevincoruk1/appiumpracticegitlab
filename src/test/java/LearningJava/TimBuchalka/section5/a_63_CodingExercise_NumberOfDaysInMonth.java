package LearningJava.TimBuchalka.section5;

public class a_63_CodingExercise_NumberOfDaysInMonth
{
    public static void main(String[] args)
    {
        getDaysInMonth(2, 2104);
        // getDaysInMonth(13,10000);
        isLeapYear(2104);
        //System.out.println("Is Leap Year "+ leapYear);
    }

    public static boolean isLeapYear(int year)
    {
        if (year < 1 || year > 9999)
        {
            System.out.println("False1");
        } else
        {
            long reminder = year % 4;
            //System.out.println("The reminder after year / 4 is " + reminder);
            if (reminder == 0)
            {
                reminder = year % 100;
                //System.out.println("The reminder after year / 100 is " + reminder);
                if (reminder == 0)
                {
                    reminder = year % 400;
                   //System.out.println("The reminder after year / 400 is " + reminder);
                    if (reminder == 0)
                    {
                        //boolean  isLeapYear = return true;
                        return true;
                    } else
                    {
                        //System.out.println("False 400");
                        return false;
                    }
                } else
                {
                    //System.out.println("true 100");
                    return true;
                }
            } else
            {
                //System.out.println("False 4");
                return false;
            }
        }
        return false;
    }


    public static int getDaysInMonth(int month, int year)
    {
        if ((month >= 1 && month <= 12) && (year > 0 && year <= 9999))
        {
            if (month == 1)
            {
                System.out.println("January " + year + " has 31 days");
                return 31;
            } else if (month == 2 && isLeapYear(year) == true)
            {
                System.out.println("February " + year + " has 29 days ");
                return 29;
            } else if (month == 2 && isLeapYear(year) == false)
            {
                System.out.println("February " + year + " has 28 days ");
                return 28;
            } else if (month == 3)
            {
                System.out.println("March " + year + " has 31 days ");
                return 31;
            } else if (month == 4)
            {
                System.out.println("April " + year + " has 30 days ");
                return 30;
            } else if (month == 5)
            {
                System.out.println("May " + year + " has 31 days ");
                return 31;
            } else if (month == 6)
            {
                System.out.println("June " + year + " has 30 days ");
                return 30;
            } else if (month == 7)
            {
                System.out.println("July " + year + " has 31 days ");
                return 31;
            } else if (month == 8)
            {
                System.out.println("August " + year + " has 31 days ");
                return 31;
            } else if (month == 9)
            {
                System.out.println("September " + year + " has 30 days ");
                return 30;
            } else if (month == 10)
            {
                System.out.println("October " + year + " has 31 days ");
                return 31;
            } else if (month == 11)
            {
                System.out.println("November " + year + " has 30 days ");
                return 30;
            } else if (month == 12)
            {
                System.out.println("December " + year + " has 31 days ");
                return 31;
            } else
            {
                return -1;
            }


        } else
        {
            System.out.println("-1");
            return -1;
        }

    }

}
//
//     1- find if a year is leap or not
//         To determine whether a year is a leap year, follow these steps:
//        1- If the year is evenly divisible by 4, go to step 2. Otherwise, go to step 5.
//        2- If the year is evenly divisible by 100, go to step 3. Otherwise, go to step 4.
//        3- If the year is evenly divisible by 400, go to step 4. Otherwise, go to step 5.
//        4- The year is a leap year (it has 366 days).
//        5- The year is not a leap year (it has 365 days).
//
//
//    2- then calculate the days of the month
