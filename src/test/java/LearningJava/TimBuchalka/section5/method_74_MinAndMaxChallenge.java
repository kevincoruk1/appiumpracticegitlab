package LearningJava.TimBuchalka.section5;

import java.util.Scanner;

public class method_74_MinAndMaxChallenge
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        int min = 0;
        int max = 0;
        boolean isFirst = true;
        //System.out.println("Enter E to exit");
        //String exit = scanner.nextLine();

        while (true)
        {
            System.out.println("Enter a number or enter letter to exit");
            boolean isInteger = scanner.hasNextInt();
            //System.out.println(" its an integer 1"+ isInteger);

            if (isInteger==true)
            {
                int inputNumber = scanner.nextInt();
                //System.out.println(" its an integer 2"+ isInteger);
                if (isFirst)
                {
                    //System.out.println(" is it first or no #1" + isFirst);
                    isFirst = false;
                    //System.out.println(" is it first or no #2" + isFirst);
                    min = inputNumber;
                    max = inputNumber;
                } else
                {
                    //System.out.println(" its not first time");
                    if (inputNumber <= min)
                    {
                        min = inputNumber;
                    } else if (inputNumber >= max)
                    {
                        max = inputNumber;
                    }

                }
            } else
            {
                System.out.println("The minimum number is " + min);
                System.out.println("The maximum number is " + max);
                break;
            }
            scanner.nextLine();

        }
        scanner.close();

    }
}

 /*
-Read the numbers from the console entered by the user and print the minimum and maximum number the user has entered.
-Before the user enters the number, print the message gEnter number:h
-If the user enters an invalid number, break out of the loop and print the minimum and maximum number.

Hint:
-Use an endless while loop.

Bonus:
-Create a project with the name MinAndMaxInputChallenge.

  */
