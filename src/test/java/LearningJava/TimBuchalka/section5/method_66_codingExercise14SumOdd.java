package LearningJava.TimBuchalka.section5;

public class method_66_codingExercise14SumOdd
{
    public static void main(String[] args)
    {
//        isOdd(0);
//        isOdd(1);
//        isOdd(2);
//        isOdd(3);
//        isOdd(4);
//        isOdd(5);
//        isOdd(6);
//        isOdd(7);
//        isOdd(8);
//        isOdd(9);
//        System.out.println("**************************************************");
        sumOdd(1, 11);

    }
    public static boolean isOdd(int number)
    {
        if((number > 0) && (number % 2 !=0 ))
        {
            //System.out.println(number+ " Is a odd number");
            return true;
        }else
        {
           // System.out.println(number+ " Is not a odd number");
            return false;
        }
    }

    public static int sumOdd(int start, int end)
    {
        if ((start > 0 ) && (end > 0 ) && (end >= start))
        {
            int sumOdd=0;
            int sumEven=0;
            for (int i= start; i<=end; i++)
                if(isOdd(i) == true)
                {
                    sumOdd +=i;
                    //System.out.println(i+" is a odd number and adding to sumodd. SumOdd is = "+sumOdd+" now");
                }else
                {
                    sumEven +=i;
                    //System.out.println(i+" is a even number and adding to sumeven. SumEven is = "+sumEven+" now");
                }
            System.out.println("sum of odd numbers is = "+sumOdd);
            return sumOdd;
        }

        System.out.println("-1");
        return -1;
    }

}

