package LearningJava.TimBuchalka.section5;

import java.util.Scanner;

public class method_74_27_InputCalculator
{
    public static void main(String[] args)
    {
        inputThenPrintSumAndAverage();
    }

    public static void inputThenPrintSumAndAverage()
    {
        Scanner myScanner = new Scanner(System.in);

        int sum = 0;
        double avg = 0;
        int count = 1;

        while (true)
        {
            System.out.println("Enter #"+count+" or a letter to exit");
            boolean isInteger = myScanner.hasNextInt();

            if (isInteger)
            {
                int myNumber = myScanner.nextInt();
                sum +=myNumber;
                avg = sum / count;
                //avg =  Math.round ((double) sum / count);
                count++;
                //System.out.println("SUM = "+sum);
                //System.out.println("AVG = "+avg);

            } else
            {

                break;
            }
            myScanner.nextLine();
        }
        System.out.println(" SUM = "+sum+" AVG = "+avg );
        myScanner.close();
    }
}

