package LearningJava.TimBuchalka.section5;

public class method_67_WhileLoopStatement
{
    public static void main(String[] args)
    {
        int count = 1;      // initilization
        while (count != 5) // condition
        {
            System.out.println("The Count value is "+count);
            count ++;      // increment
        }
        // while true
        int number =1;
        while (true)
        {
            if (number == 5)
            {
                break;
            }
            System.out.println("The number values is "+number);
            number++;
        }
        // do while. it will execute at least one then check the condition at the end
        int doo=1;
        do
        {
            System.out.println("The doo value is "+doo);
            doo++;
        } while (doo<5);


        int startNumber=4;
        int finishNumber=20;
        int countOfEvenNumbers=0;

        while (startNumber <= finishNumber)
        {
            startNumber++;
            if (isEvenNumber(startNumber)==false) // this can be achieved as,  if (!isEvenNumber(startNumber))
            //if (!isEvenNumber(startNumber))
            {
              continue;
            }
            countOfEvenNumbers++;
            System.out.println("Even Number "+startNumber);
            if (countOfEvenNumbers == 5)
            {
                break;
            }
        }
        System.out.println("The total count of even number is = "+countOfEvenNumbers);


    }
    public static boolean isEvenNumber(int number)
    {
        if ((number % 2) ==0)
        {
           // System.out.println("True");
            return true;
        } else
        {
           // System.out.println("False");
            return false;
        }
    }

}

