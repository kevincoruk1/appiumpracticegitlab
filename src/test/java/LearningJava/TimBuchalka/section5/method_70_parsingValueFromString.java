package LearningJava.TimBuchalka.section5;

public class method_70_parsingValueFromString
{
    public static void main(String[] args)
    {
        // this is a string
        // if you add a latter to this string (like 2018a), you will receive exception error,
        // it will not parse into integer
        String numberAsString = "2018";
        System.out.println("The string "+numberAsString+" is parsed into a integer as :"+numberAsString);

        // parse the string into a integer
        int stringAsNumber =Integer.parseInt(numberAsString);
        System.out.println("The string "+numberAsString+" is parsed into a integer as :"+stringAsNumber);

        // parse the string into a double
        double stringAsDouble =Double.parseDouble(numberAsString);
        System.out.println("The string "+numberAsString+" is parsed into a double as :"+stringAsDouble);

        // when you try to add an integer to string, it will append to the end of the string.
        numberAsString +=1;  //-->>> will display 20181
        System.out.println(" adding a integer to a string will append the string : "+ numberAsString);
        // when you try to add integer to a string that is parsed into a integer it will sum;
        stringAsNumber +=1; //--->> will display 2019
        System.out.println(" adding a integer to a string that is parsed into a integer will sum "+ stringAsNumber);
    }


}

