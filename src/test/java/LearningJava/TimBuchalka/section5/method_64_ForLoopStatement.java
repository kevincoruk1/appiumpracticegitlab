package LearningJava.TimBuchalka.section5;

public class method_64_ForLoopStatement
{
    public static void main(String[] args)
    {
//        System.out.println("10,000 at 1% interest rate = "+calculateInterest(10000,1));
//        System.out.println("10,000 at 2% interest rate = "+calculateInterest(10000,2));
//        System.out.println("10,000 at 3% interest rate = "+calculateInterest(10000,3));
//        System.out.println("10,000 at 4% interest rate = "+calculateInterest(10000,4));
//        System.out.println("10,000 at 5% interest rate = "+calculateInterest(10000,5));
//
//       // for (initialization; termination; increment)
//       // for (int i=1; i<6; i++)
//        for (double interestRate =1; interestRate<6; interestRate++)
//        {
//            System.out.println( "10,000 at "+interestRate+ "% interest ="+ calculateInterest(10000,interestRate));
//        }
//       //we can do the same as following for loop
//        for(int i=1; i<10; i++)
//        {
//            System.out.println( "10,000 at "+i+ "% interest ="+ calculateInterest(10000,i));
//            //to format the result
//            System.out.println( "10,000 at "+i+ "% interest ="+ String.format("%.2f",calculateInterest(10000,i)));
//        }
//        // counting backward
//        for(int i=10; i>0; i--)
//        {
//            System.out.println( "10,000 at "+i+ "% interest ="+ calculateInterest(10000,i));
//        }


//    }
//    public static double calculateInterest(double amount, double interestRate)
//    {
//        return (amount * (interestRate / 100));


     // isPrime exercise
     // isPrime(11);
      int count=0;
      for (int i=1; i<50; i++)
        {
            if (isPrime(i)== false)
             {
              //System.out.println("False "+i+ " is not a prime number");
             } else if(isPrime(i)== true)
             {
              System.out.println("True "+i+ " is a prime number");
              count++;
              if (count==3)
              {
                  break;
              }
             }

        }
        System.out.println("Found Total of "+count+ " Prime Numbers");

      // or
        int count1=0;
        for (int i=1; i<50; i++)
        {
            if (isPrime(i))
            {
               count1++;
               System.out.println("Number "+i+ " is a prime number");
                if (count1==3)
                {
                    break;
                }
            }

        }
        System.out.println("Found Total of "+count1+ " Prime Numbers");


    }

    public static boolean isPrime(int n)
    {
        if (n==1)
        {
            //System.out.println("False "+n+ " is not a prime number");
            return false;
        }
        for (int i=2; i <= n/2; i++)
        {
            if (n % i ==0)
            {
                //System.out.println("False "+n+ " is not a prime number");
                return false;
            }
        }
        //System.out.println("True "+n+ " is a prime number");
        return true;
    }






}

