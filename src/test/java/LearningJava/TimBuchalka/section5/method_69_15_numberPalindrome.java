package LearningJava.TimBuchalka.section5;

public class method_69_15_numberPalindrome
{
    public static void main(String[] args)
    {
        isPalindrome (1221);
    }


        // method to find if a reverse of the number is equal to its original value like 121 =121
    public static boolean isPalindrome(int number)
    {
        //int originalNumber =0;
        int reverseNumber = 0;
        int lastDigit = 0;
        int originalNumber = number;

        while (number > 0)
        {
            lastDigit = number % 10;
            System.out.println("The last digit " + lastDigit);

            reverseNumber = (reverseNumber * 10);  //+ lastDigit;
            System.out.println("The reverse number " + reverseNumber);

            reverseNumber += lastDigit;
            System.out.println("The reverse number + last digit  " + reverseNumber);

            number = number / 10;
            System.out.println("The number " + number);
            System.out.println("*************************************************");
        }
            if(originalNumber == reverseNumber)
            {
                System.out.println("The reverse of "+originalNumber + " = "+reverseNumber);
                return true;
            }else
            {
                System.out.println("The reverse of "+originalNumber + " is not = "+reverseNumber);
                return false;
            }
    }

}

