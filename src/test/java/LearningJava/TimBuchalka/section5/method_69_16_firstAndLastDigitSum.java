package LearningJava.TimBuchalka.section5;

public class method_69_16_firstAndLastDigitSum
{
    public static void main(String[] args)
    {
        sumFirstAndLastDigit (5);

    }

    public static int sumFirstAndLastDigit(int number)
    {
        int firstNumber = number;
        int lastNumber = number % 10;
        int sum = 0;

        if (number < 0)
        {
            System.out.println("-1");
            return -1;
        } else
        {
            while (firstNumber >= 10)
            {
                firstNumber = firstNumber / 10;
                System.out.println("first number = " + firstNumber);

                System.out.println("last number  = " + lastNumber);

                System.out.println("******************");
            }
            sum = firstNumber + lastNumber;
            System.out.println("The sum of " + firstNumber + " and " + lastNumber + " is = " + sum);
            return sum;
        }
    }
}

