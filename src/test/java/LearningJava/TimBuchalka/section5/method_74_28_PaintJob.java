package LearningJava.TimBuchalka.section5;

public class method_74_28_PaintJob
{
    public static void main(String[] args)
    {
        getBucketCount(0.75, 0.75, 0.5, 0);
        getBucketCount(3.4, 2.1, 1.5, 2);
        getBucketCount(2.75, 3.25, 2.5, 1);
//
//        getBucketCount(-3.4, 2.1, 1.5);
//        getBucketCount(3.4, 2.1, 1.5);
//        getBucketCount(7.25, 4.3, 2.35);

//        getBucketCount(3.4,1.5);
//        getBucketCount(6.26, 2.2);
//        getBucketCount(3.26, 0.75);


    }

    public static int getBucketCount(double width, double height, double areaPerBucket, int extraBucket )
    {
        double area= 0;
        int numberOfBucketNeed = 0;

        if (width <= 0 || height <= 0 || areaPerBucket <= 0 || extraBucket < 0)
        {
            System.out.println("Invalid parameter/s -1");
            return -1;
        } else
        {
            area = width * height;
            numberOfBucketNeed = (int)(Math.ceil (  area / areaPerBucket ) - extraBucket);
        }
        System.out.println("Bob Needs "+numberOfBucketNeed+" more number of buckets to paint area of "+area);
        return numberOfBucketNeed;
    }
    public static int getBucketCount(double width, double height, double arePerBucket )
    {
        double area= 0;
        int numberOfBucketNeed = 0;

        if (width <= 0 || height <= 0 || arePerBucket <= 0)
        {
            System.out.println("Invalid parameter/s -1");
            return -1;
        } else
        {
            area = width * height;
            numberOfBucketNeed = (int) (Math.ceil ( area / arePerBucket ));
        }
        System.out.println("Bob Needs "+numberOfBucketNeed+" more number of buckets to paint area of "+area);
        return numberOfBucketNeed;
    }
    public static int getBucketCount(double area, double arePerBucket )
    {
        int numberOfBucketNeed = 0;

        if (area <= 0 || arePerBucket <= 0)
        {
            System.out.println("Invalid parameter/s -1");
            return -1;
        } else
        {
            numberOfBucketNeed = (int) (Math.ceil ( area / arePerBucket ));
        }
        System.out.println("Bob Needs "+numberOfBucketNeed+" more number of buckets to paint area of "+area);
        return numberOfBucketNeed;
    }
}

