package LearningJava.TimBuchalka.section8;
import java.util.Scanner;


public class method_98_Arrays
{

    private static Scanner scanner = new Scanner (System.in);

    public static void main(String[] args)
    {
        // defining a normal variable
        int myVariable = 10;

        // defining a array #1
        int [] myIntArray_1;
        myIntArray_1 = new int [10];
        // defining a array #2
        int [] myIntArray_2 = new int [10];
        // defining a array #3 and assigning value at the same time.
        int [] myIntArray_3 = {1,2,3,4,5,6,7,8,9,10};
//*************************************************************
        // assigning value to element in a array with for loop.
        int [] myIntArray_4 = new int [10];
        for (int i =0; i<10; i++)
        {
            myIntArray_4[i] = i;
        }
        // calling those arrays element
        for (int i = 0; i<10; i++)
        {
            System.out.println("For element "+i+", the value of array is = "+myIntArray_4[i]);
        }
//*************************************************************
        int [] myIntArray_5 = new int [10];
        for (int i =0; i<myIntArray_5.length; i++)
        {
            myIntArray_5[i] = i;
        }
        // calling those arrays element
        for (int i = 0; i<myIntArray_5.length; i++)
        {
            System.out.println("This is with array.lenght method. For element "+i+", the value of array is = "+myIntArray_5[i]);
        }


        // calling print method
        printArray(myIntArray_5);

        // the below code is referancing to the getInputArray method

        int[] myInputArrays = getInputArray(10);
        for (int i =0; i< myInputArrays.length; i++)
        {
            System.out.println("This is with inputArray method. Element "+i+ ", the value of array is ="+myInputArrays[i]);
        }

    }

    // create a print method
    //int [] myIntArray_6 = new int [10];
    public static void printArray(int[] myIntArray_6)
    {
        for (int i = 0; i<myIntArray_6.length; i++)
            System.out.println("This is with print array method. Element "+i+ ", the value of array is = "+myIntArray_6[i]);
    }

    // creating an input method to assigning array
    public static int [] getInputArray(int number)
    {
        System.out.println("Enter "+number+" integers;");
        int [] values = new int [number];

        for (int i = 0; i<values.length; i++)
        {
            values[i] = scanner.nextInt();
        }
        return values;
    }

}

