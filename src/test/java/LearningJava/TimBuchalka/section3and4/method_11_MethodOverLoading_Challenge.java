package LearningJava.TimBuchalka.section3and4;

public class method_11_MethodOverLoading_Challenge
{
    public static void main(String[] args)
    {
        calcFeetAndInchesToCentimeters(6, 0);
        calcFeetAndInchesToCentimeters(157);

    }


    public static double calcFeetAndInchesToCentimeters(double feet, double inches)
    {
      if (feet >=0  && (inches >=0 && inches <=12))
      {
          double convertInchesToCm =((feet * 12 )+ inches) * 2.54;
          System.out.println(feet+ " Feet, "+ inches+" inches = "+convertInchesToCm+" cm");
          return convertInchesToCm;
      } else
      {
          System.out.println("False -1");
          return -1;
      }
    }

    public static double calcFeetAndInchesToCentimeters(double inches)
    {
        if (inches >=0 )
        {
           double feet = (int) inches / 12;
           double remainingInches =(int) inches % 12;

            System.out.println(inches+ " Inches = "+feet+" feet and "+remainingInches+" inches");
           // this is calling the first method with 2 parameters but its using the parameters that is calculated in this method
            return calcFeetAndInchesToCentimeters(feet, remainingInches );
        } else
        {
            System.out.println("False -1");
            return -1;
        }
    }

}


