package LearningJava.TimBuchalka.section3and4;

public class operators
{
    public static void main(String[] args)
    {
        int result;
        result = 1 + 2; //3
        System.out.println("1+2 = " + result);
        result = result + 1;//3+1=4
        System.out.println("3+1 = " + result);
        result++; //4+1=5
        System.out.println("4+1 = " + result);
        result--; //5-1=4
        System.out.println("4-1 = " + result);
        result += 5; //4+5=9
        System.out.println("4+5 = " + result);
        result *= 2; //9*2=18
        System.out.println("9*2 = " + result);

        int iValue = 8;
        double dValue = 8;
        System.out.println(iValue + " mod 3 = " + iValue % 3); // the reminder of 8/3 =2
        System.out.println(dValue + " mod 3 = " + dValue % 3); // the reminder of 8/3 =2.0


    }

}
