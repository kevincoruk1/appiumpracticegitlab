package LearningJava.TimBuchalka.section3and4;

public class method_15_EqualityPrinter
{
    public static void main(String[] args)
    {
        printEqual(0,0,-1);
    }
    public static void printEqual(int type1, int type2, int type3)
    {
       if(type1 >= 0 && type2 >= 0 && type3 >=0)
        {
          if (type1 ==  type2 && type2 == type3)
          {
              System.out.println("All numbers are equal");
          } else if (type1 !=  type2 && type2 != type3 && type3 != type1)
        {
            System.out.println("All numbers are different");
        } else
          {
              System.out.println("Neither all are equal or different");
          }
         } else
        {
            System.out.println("Invalid Value");
        }
    }

}

