package LearningJava.TimBuchalka.section3and4;

public class dataTypes
{

    public static void main(String[] args)
    {
        //int
        int myMinIntValue = Integer.MIN_VALUE; //-2147483648
        int myMaxIntValue = Integer.MAX_VALUE; //2147483647
        System.out.println("The minimum int value is " + myMinIntValue);
        System.out.println("The maximum int value is " + myMaxIntValue);
        //byte
        byte myMinByteValue = Byte.MIN_VALUE; //-128
        byte myMaxByteValue = Byte.MAX_VALUE; //127
        System.out.println("The minimum byte value is " + myMinByteValue);
        System.out.println("The maximum byte value is " + myMaxByteValue);

        //short
        short myMinShortValue = Short.MIN_VALUE; //-32768
        short myMaxShortValue = Short.MAX_VALUE; //32767
        System.out.println("The minimum short value is " + myMinShortValue);
        System.out.println("The maximum short value is " + myMaxShortValue);
        //short bigShortLiteralValue = 32768;// this give error as its bigger than the max value of short type
        //long
        long myLongValue = 100L;
        long myMinLongValue = Long.MIN_VALUE; // -9223372036854775808
        long myMaxLongValue = Long.MAX_VALUE; // 9223372036854775807
        System.out.println("The minimum long value is " + myMinLongValue);
        System.out.println("The maximum long value is " + myMaxLongValue);

        //long bigLongLiteralValue=2147483647123; //this will give error "integer number too large"
        long bigLongLiteralValue1 = 2147483647123L; //putting L in the end will solve the issue

        int myTotal = (myMinIntValue / 2);
        // the below line receives error because we define the variable "myNewByteValue" as byte and result of (myMinIntValue / 2) is not byte anymore its "int
        // byte myNewByteValue=(myMinIntValue / 2);

        // to solve above issue use casting; change the one variable type to another just put the variable type into parenthesis
        byte myNewByteValue1 = (byte) (myMinIntValue / 2);

        //challenge
        byte variable1 = 126;
        short variable2 = 12345;
        int variable3 = 123456;
        long variable4 = 50000 + 10 * (variable1 + variable2 + variable3);
        //for short type data, we have to caste the result of the variable to short.
        short variable5 = (short) (50000 + 10 * (variable1 + variable2 + variable3));
        System.out.println(variable4);


        //float & double: numbers that have fractional part that we express with a decimal point like 3.14
        float myMinFloatValue = Float.MIN_VALUE; // 1.4E-45
        float myMaxFloatValue = Float.MAX_VALUE; //3.4028235E38
        System.out.println("The minimum float value is " + myMinFloatValue);
        System.out.println("The maximum float value is " + myMaxFloatValue);

        double myMinDoubleValue = Double.MIN_VALUE; //4.9E-324
        double myMaxDoubleValue = Double.MAX_VALUE; //1.7976931348623157E308
        System.out.println("The minimum double value is " + myMinDoubleValue);
        System.out.println("The maximum double value is " + myMaxDoubleValue);

        int myIntValue = 5;
        float myFloatValue = 5; // or you can say =5f to indicate its a float (optional here)
        double myDoubleValue = 5; // or you can say =5d to indicate its a double (optional here)

        int myIntValue1 = 5;// int is a whole number
        // the below line will give you error bc we define the value as float but the result is double so we have to use casting
        //float myFloatValue1=5.2;
        //to oslve above issue we have to use casting or put "f" next to the double number
        float myFloatValue2 = (float) 5.2;
        // or
        float myFloatValue3 = 5.12344123456f; //print only 6 digit after decimal point.
        double myDoubleValue1 = 5.1234567890123456789;//print 15 digit after decimal point
        System.out.println("The float value is " + myFloatValue3 + " and double value is " + myDoubleValue1);

        //for float&double data type you have to use f and d if the number is a whole number and want the result to be decimal.
        // if you dont use f and d then the result will be 1 digit decimal
        int x = 5 / 3; //=1
        float y = 5f / 3f; //=1.6666666
        double z = 5d / 3d; //=1.6666666666666667
        System.out.println("The integer is whole number like this " + x);
        System.out.println("The float is with 6 digit decimal like this number " + y);
        System.out.println("The double is with 16 digit like this number " + z);

        //for double data type you can remove the "d" and use decimal point instead
        double zz = 5.0 / 3.0; //=1.6666666666666667

        // challenge: convert pound to kg
        int pound = 200;
        double poundToKg = 0.45359237;
        double convert = pound * poundToKg;
        System.out.println(pound + " pound = " + convert + " kg");

        //char: single character
        // it only accept single digit; cant type 2 latter
        char myChar = 'D';
        // get the char by unicode: https://unicode-table.com/en/
        // for D, first locate the row where the D is and get the first 3 digit of row number then get the column number
        char myUnicodeChar = '\u0044'; //=D
        char myUnicodeChar1 = '\u004B'; //=K
        char myUnicodeChar2 = '\u0040'; //=@
        System.out.println("This is a single char " + myChar);
        System.out.println("This is a single char " + myUnicodeChar);
        System.out.println("This is a single char " + myUnicodeChar1);
        System.out.println("This is a single char " + myUnicodeChar2);
        // boolean accept 2 values only, true or false, yes or no, 1 or 0
        boolean myTrueBooleanValue = true;
        boolean myFalseBooleanValue = false;
        //String: any data between a double quote ? ""
        String myName = "Kevin";
        myName = "My full name is " + myName + " Coruk";
        System.out.println(myName);

        String myString = "This is a string";
        myString = myString + " 250.50";
        myString = myString + "10";
        int myInt = 10;
        myString = myString + myInt;
        double myDouble = 10.55;
        myString = myString + myDouble;

        System.out.println(myString);


    }

}
