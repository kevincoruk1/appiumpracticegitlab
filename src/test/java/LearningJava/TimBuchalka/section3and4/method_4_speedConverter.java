package LearningJava.TimBuchalka.section3and4;

public class method_4_speedConverter
{
    public static void main(String[] args)
    {
        toMilesPerHour(17);
        //printCoversion(kilometersPerHour);
        toMilesPerHour(29);
    }

    public static long toMilesPerHour(double kilometersPerHour)
    {
        if (kilometersPerHour < 0)
        {
            System.out.println("-1");
            //return - 1;
        } else
        {
            long milesPerHour = Math.round(kilometersPerHour * 0.621371);
            System.out.println(milesPerHour);
            //return Math.round(kilometersPerHour / 1.609);

        }
        return -1;
    }

    public static void printCoversion(double kilometersPerHour)
    {
        if (kilometersPerHour < 0)
        {
            System.out.println("Invalid Value");
        } else
        {
            long milesPerHour = toMilesPerHour(kilometersPerHour);
            System.out.println(kilometersPerHour + "km/h = " + milesPerHour + " mi/h");
        }
    }


}

