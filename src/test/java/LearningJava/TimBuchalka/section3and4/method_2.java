package LearningJava.TimBuchalka.section3and4;

public class method_2
{
    public static void main(String[] args)
    {
         //call the first one
        int highScorePosition = calculateHighScorePosition_1(100);
        displayHighScorePosition("Tim_1", highScorePosition);
        highScorePosition = calculateHighScorePosition_1(10);
        displayHighScorePosition("Kevin_1", highScorePosition);
        //call the second one
        int highScorePosition_2 = calculateHighScorePosition_2(100);
        displayHighScorePosition("Tim_2", highScorePosition_2);
        highScorePosition_2 = calculateHighScorePosition_2(10);
        displayHighScorePosition("Kevin_2", highScorePosition_2);

    }


    public static void displayHighScorePosition(String playerName, int highScorePosition)
    {
        System.out.println(playerName + " managed to get into position " + highScorePosition + " on the high score table");
    }

    //first way of doing the calculation
    public static int calculateHighScorePosition_1(int playerScore)
    {
        if (playerScore >= 1000)
        {
            return 1;
        } else if (playerScore >= 500 )
        {
            return 2;
        } else if (playerScore >= 100 )
        {
            return 3;
        }
            return 4;
    }
     // second way of doing the calculation
    public static int calculateHighScorePosition_2(int playerScore)
    {
        int position =4;
        if (playerScore >= 1000)
        {
            position = 1;
        } else if (playerScore >= 500 )
        {
            position = 2;
        } else if (playerScore >= 100 )
        {
            position= 3;
        }
        return position;
    }


}

