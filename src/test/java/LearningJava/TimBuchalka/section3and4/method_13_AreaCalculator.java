package LearningJava.TimBuchalka.section3and4;

public class method_13_AreaCalculator
{
    public static final double INVALID_MESSAGE = -1.0;

    public static void main(String[] args)
    {
        area(1);
        area(0,0);
    }

    public static double area(double radius)
    {
        if (radius >= 0)
        {
            double areaOfCircle = radius * radius * Math.PI;
            System.out.println("The area of the circle with " + radius + " radius is " + areaOfCircle);
            return areaOfCircle;
        } else
            System.out.println(INVALID_MESSAGE);
        return INVALID_MESSAGE;
    }

    public static double area(double x, double y )
    {
        if (x >= 0 && y >= 0)
        {
            double areaOfRectangle = x * y;
            System.out.println("The area of the rectangle  with " + x + " and " + y+ " = "+areaOfRectangle);
            return areaOfRectangle;
        } else
            System.out.println(INVALID_MESSAGE);
        return INVALID_MESSAGE;
    }


}

