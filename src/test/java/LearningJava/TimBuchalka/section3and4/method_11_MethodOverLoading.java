package LearningJava.TimBuchalka.section3and4;

public class method_11_MethodOverLoading
{
    public static void main(String[] args)
    {
        // first method
        calculateScore ("Kevin", 100);
        // second method. once you start typing the name of the method,
        // the intellij will suggest all the method so you can choose which method to use; with one parameters or with two parameters.
        calculateScore (200);
        // third method
        calculateScore ();

        // we can even create a new score by using the return statement in the method. (the return will multiply the score by 1000
        int newScore =calculateScore("Tim", 150);
        // wil print 150*1000=150000
        System.out.println(newScore);

        // this will first run the method with Bob & 100 parameters and then it will print the return of the calculateScore
        // which is 100*1000
        System.out.println(calculateScore("Bob", 100));

    }
    // first method
    public static int calculateScore(String playerName, int score)
    {
        System.out.println("First method. Player " +playerName+" scored "+score+" points");
        return score * 1000;
    }
    // second method
    // we can change the parameters in the method to be able to use the same method name
    public static int calculateScore(int score)
    {
        System.out.println("Second method. Unnamed player scored "+score+" points");
        return score * 1000;
    }

    // third method
    // we can even have the same method without any parameters
    public static int calculateScore()
    {
        System.out.println("Third method. No player name, no player score ");
        return 0;
    }
}


