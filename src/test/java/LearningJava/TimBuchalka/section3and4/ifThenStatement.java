package LearningJava.TimBuchalka.section3and4;
public class ifThenStatement
{
    public static void main(String[] args)
    {
        boolean isAlien = false;
        // if a=b
        if (isAlien == false)  //if you put; here it will execute the next line no mather what; meaning it will not check for the condition
            System.out.println("It is not an alien"); // It is not an alien

        if (isAlien == true)
            ; //if you put; here it will execute the next line no mather what. meaning it will not check for the condition
        System.out.println("It is not an alien");

        // the first line after if statement will not be printed because the if condition is not meet, however it will print the second line after if.
        if (isAlien == true)
            System.out.println("It is not an alien");
        System.out.println("That's good. i am scared of aliens :)");
        int firstScore = 50;
        int secondScore = 75;

        if (firstScore == 50)
        {
            System.out.println("First score is 50");
        }
        //if a not = b
        if (firstScore != 51) //not equal to 50
        {
            System.out.println("First score is Not 50");
        }
        //if a<b and b<x can be written as (a<b) and (b<x)
        if (firstScore < secondScore && secondScore > 50) //if First score is less than second score and second score is less than 100
        {
            System.out.println("First score is less than second score and second score is greater than 50");
        }
        //if a<b or b<x
        if ((firstScore == secondScore) || (secondScore < 100)) //if First score is less than second score and second score is less than 100
        {
            System.out.println("Either or both conditions are true");
        }


        //challenge
        //1
        double myDouble1 = 20.00d;
        //2
        double myDouble2 = 80.00d;
        //3  add both numbers and multiplay with 100
        double myTotalDouble = (myDouble1 + myDouble2) * 100.00;
        System.out.println("The total of 2 numbers miltiplayed with 100 is " + myTotalDouble);
        //4 find the reminder of myTotalDouble / 40.00
        double mydevider = 40.00;
        double myReminder = myTotalDouble % mydevider; //
        System.out.println("The reminder of " + myTotalDouble + " / " + mydevider + " is " + myReminder);
        //5  create a boolean variable and assign true if the reminder is #4 is 0 else assign false
        boolean isReminderBoolean = (myReminder == 0) ? true : false;
        //6 print the boolean value
        System.out.println("The boolean is " + isReminderBoolean);
        //7 write if- then statement to print "Got some reminder" if the boolean in step 5 is not "true"

        if (isReminderBoolean == false)
        {
            System.out.println("Got some reminder");
        }

    }
}
