package LearningJava.TimBuchalka.section3and4;

public class method_1
{

    public static void main(String[] args)
    {
        // 2 second call the method from below #1 and pass the value
        calculateScore(true, 800, 5, 100);
        calculateScore(true, 10000, 8, 200);

    }
    // 1 first create the method here with its variables
    public static void calculateScore(boolean gameOver, int score, int levelCompleted, int bonus)
    {
        if (gameOver)
        {
            int finalScore = score + (levelCompleted * bonus);
            finalScore += 2000;
            System.out.println("Your final score was " + finalScore);
        }
    }

    public static void displayHighScorePosition(String playerName, int highScorePosition)
    {
        System.out.println(playerName + " managed to get into position " + highScorePosition + " on the high score table");
    }

}

