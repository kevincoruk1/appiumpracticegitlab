package LearningJava.TimBuchalka.section3and4;

public class method_5_MegaBytesconverter
{
    public static void main(String[] args)
    {
        printMegaBytesAndKiloBytes(2500);
    }

    public static void printMegaBytesAndKiloBytes (int kiloBytes)
    {
       if(kiloBytes < 0)
       {
           System.out.println("Invalid Value");
       }else
       {
          int convertKBToMB = kiloBytes / 1024;
          int reminderKB = kiloBytes % 1024;
          System.out.println(kiloBytes+ " KB = " +convertKBToMB+ " MB and "+ reminderKB+" KB");
       }
    }


}

