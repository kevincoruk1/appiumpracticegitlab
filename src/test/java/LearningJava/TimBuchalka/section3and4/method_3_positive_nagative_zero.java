package LearningJava.TimBuchalka.section3and4;


// create a method that will return
// positive if he parameter is >0
// negative if the parameter is <0
// zero if the parameter is =0
public class method_3_positive_nagative_zero
{
    public static void main(String[] args)
    {
        checkNumber(5);
        checkNumber(-5);
        checkNumber(0);
    }

    public static void checkNumber(int number)
    {
        if (number>0)
            System.out.println("positive");
        else if (number<0)
            System.out.println("negative");
        else if (number==0)
            System.out.println("zero");
    }

}

