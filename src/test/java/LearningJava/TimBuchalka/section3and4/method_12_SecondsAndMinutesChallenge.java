package LearningJava.TimBuchalka.section3and4;

public class method_12_SecondsAndMinutesChallenge
{
    public static void main(String[] args)
    {
        // we can use System.out.println to pass values into parameters as well.
        //System.out.println(getDurationString(61,0));

        getDurationString(65,8);
        getDurationString(3945);

    }
    public static String getDurationString(long minutes, long seconds)
    {
        if (minutes >=0 && (seconds >=0 && seconds <=59))
        {
            long hours = minutes / 60;
            long remainingminutes =  minutes % 60;

            //if we want to get the hours and minus as 2 digit like 01h, 01 min
            String hoursString =hours + "h";
            if (hours<10)
            {
                hoursString = "0"+ hoursString;
            }
            String minutesString =remainingminutes + "m";
            if (remainingminutes<10)
            {
                minutesString = "0"+ minutesString;
            }
            String secondString =seconds + "s";
            if (seconds<10)
            {
                secondString = "0"+ secondString;
            }



            System.out.println(minutes+ " Minutes and "+ seconds+" Seconds = "+hoursString+ " "+minutesString+" "+secondString);
           // return hours +"h" +remainingminutes+"m "+seconds+"s"; //1h 5m 45s
        }else
            System.out.println("Invalid Value");
            return "Invalid Value";
    }

    public static String getDurationString(long seconds)
    {
        if (seconds >=0 )
        {
            long minutes = seconds / 60;
            long remainingSeconds = seconds % 60;

            System.out.println(seconds+ " Seconds = "+minutes+"m "+remainingSeconds+"s");
            return getDurationString(minutes,remainingSeconds);
        }else
            System.out.println("Invalid Value");
            return "Invalid Value";
    }

}

/*
    Create a method called getDurationString with two parameters, first parameter minutes and 2nd parameter seconds.

        You should validate that the first parameter minutes is >= 0.

        You should validate that the 2nd parameter seconds is >= 0 and <= 59.

        The method should return gInvalid valueh in the method if either of the above are not true.

        If the parameters are valid then calculate how many hours minutes and seconds equal the minutes and seconds passed to this method and return that value as string in format gXXh YYm ZZsh where XX represents a number of hours, YY the minutes and ZZ the seconds.

        Create a 2nd method of the same name but with only one parameter seconds.

        Validate that it is >= 0, and return gInvalid valueh if it is not true.

        If it is valid, then calculate how many minutes are in the seconds value and then call the other overloaded method passing the correct minutes and seconds calculated so that it can calculate correctly.

        Call both methods to print values to the console.

        Tips:
        Use int or long for your number data types is probably a good idea.
        1 minute = 60 seconds and 1 hour = 60 minutes or 3600 seconds.
        Methods should be static as we have used previously.

        Bonus:
        For the input 61 minutes output should be 01h 01m 00s, but it is ok if it is 1h 1m 0s (Tip: use if-else)
        Create a new console project and call it SecondsAndMinutesChallenge
*/