package LearningJava.TimBuchalka.section3and4;

public class method_14_MinutesToYearsAndDaysCalculator
{
    public static String INVALID_VALUE= "Invalid Value";
    public static void main(String[] args)
    {
        printYearsAndDays(561600);
        printYearsAndDays(527105);
    }
    public static void printYearsAndDays(long minutes)
    {
      if (minutes >= 0 )
      {
          long minutesToHours = minutes / 60;
          long remainingMinutes = minutes % 60;

          long minutesToDays = minutesToHours / 24;
          long remainingHours = minutesToHours % 24;

          long minutesToYear = minutesToDays / 365;
          long remainingDays = minutesToDays % 365;

          System.out.println(minutes+" min = "+minutesToYear+" y and "+remainingDays+" d" );
          //System.out.println(minutes+" min = "+minutesToYear+" y and "+remainingDays+" d"+ " and "+remainingHours+" h "+remainingMinutes+" m" );
      } else
      {
          System.out.println(INVALID_VALUE);
          //return INVALID_VALUE;
      }
    }

}

