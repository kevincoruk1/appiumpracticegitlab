package LearningJava.DheeruMundluru;

public class ScopeOfVariables
{
  static int x = 0; // class variable
  static int y = 4;
         int z= 0;  // can not access in the method
  int sum = 0;

    public static void main(String[] args)
    {
      int x1= 5; // local varaible
      System.out.println("Main method can access variable defined in main method like 'x1' "+x1);
      System.out.println("Main method can not access variable defined in 'class' level if its defined as'static'like x "+x);
      //System.out.println("Main method can not access 'z' because its not defined as 'static' like "+z);
    }


    public static void method1()
    {
      int x2= 5;
      System.out.println("Main method can access variable defined in main method like 'x2' "+x2);
      System.out.println("Main method can not access variable defined in 'class' level if its defined as'static'like x "+x);
      //System.out.println("Main method can not access 'z' because its not defined as 'static' like "+z);

    }

  public static void method2()
  {


  }


}
