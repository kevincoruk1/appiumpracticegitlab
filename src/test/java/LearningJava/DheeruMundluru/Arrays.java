package LearningJava.DheeruMundluru;

public class Arrays
{

    public static void main(String[] args)
    {
        //declaring/initialize an array #1
        int [] scores1 = {90,70,80,100};

        System.out.println("array #1 Mid term 1 score: "+ scores1[0]);
        System.out.println("array #1 Mid term 2 score: "+ scores1[1]);
        System.out.println("array #1 Final score: "+ scores1[2]);
        System.out.println("array #1 Project score: "+ scores1[3]);
        System.out.println("array length: "+ scores1.length);

        System.out.println("*********************************************");

        //declaring/initialize an array #2
        int [] scores2 = new int[] {90,70,80,100};

        System.out.println("array #2 Mid term 1 score: "+ scores2[0]);
        System.out.println("array #2 Mid term 2 score: "+ scores2[1]);
        System.out.println("array #2 Final score: "+ scores2[2]);
        System.out.println("array #2 Project score: "+ scores2[3]);

        System.out.println("*********************************************");

        //declaring an array #3
        int [] scores3 = new int [4];

        // initialize the array
        scores3[0] = 90;
        scores3[1] = 70;
        scores3[2] = 80;
        scores3[3] = 100;
        System.out.println("array #3 Mid term 1 score: "+ scores3[0]);
        System.out.println("array #3 Mid term 2 score: "+ scores3[1]);
        System.out.println("array #3 Final score: "+ scores3[2]);
        System.out.println("array #3 Project score: "+ scores3[3]);

        System.out.println("*********2D Arrays ********************************");

        String [][] my2dArray0= new String [3][4]; //[rows] [columns]
        // first row
        my2dArray0[0][0] = "row 0 column 0";
        my2dArray0[0][1] = "row 0 column 1";
        my2dArray0[0][2] = "row 0 column 2";
        my2dArray0[0][3] = "row 0 column 3";
        // second row
        my2dArray0[1][0] = "row 1 column 0";
        my2dArray0[1][1] = "row 1 column 1";
        my2dArray0[1][2] = "row 1 column 2";
        my2dArray0[1][3] = "row 1 column 3";
        // third row
        my2dArray0[2][0] = "row 2 column 0";
        my2dArray0[2][1] = "row 2 column 1";
        my2dArray0[2][2] = "row 2 column 2";
        my2dArray0[2][3] = "row 2 column 3";


        System.out.println("row zero, column zero= "+ my2dArray0[0][0]);
        System.out.println("row two, column three= "+ my2dArray0[2][3]);


        System.out.println("******************my2dArray1***************************");

        int [][] my2dArray1 = new int [][]
                {
                        {2,3,4}, //row 0
                        {5,6,7}, // row 1
                        {8,9,10} // row 2
                }; //3 rows 3 columns
        System.out.println("my2dArray1 row zero, column zero= "+ my2dArray1[0][0]); //2
        System.out.println("my2dArray1 row zero, column one= "+ my2dArray1[0][1]); //3
        System.out.println("my2dArray1 row zero, column two= "+ my2dArray1[0][2]); //4
        System.out.println("my2dArray1 row one, column one= "+ my2dArray1[1][1]); //5

        System.out.println("****************my2dArray2*****************************");

        int [][] my2dArray2 = {{2,3},{4,5}}; //2 rows 2 columns
        System.out.println("my2dArray2 row zero, column zero= "+ my2dArray2[0][0]); //2
        System.out.println("my2dArray2 row zero, column one= "+ my2dArray2[0][1]); //3
        System.out.println("my2dArray2 row one, column zero= "+ my2dArray2[1][0]); //4
        System.out.println("my2dArray2 row one, column one= "+ my2dArray2[1][1]); //5
        //System.out.println("my2dArray1 ccrow zero, column zero= "+ my2dArray1[2][0]); // out of bound

        System.out.println("****************my2dArray3 Irregular Rows *****************************");
        int [][] my2dArray3 = new int [][]
        {
            {2,3,4}, //row 0
            {5,6,7,8},// row 1
            {8,9,10,11,12} // row 2
        };

        System.out.println("my2dArray3 row zero, column zero= "+ my2dArray3[0][0]); //2
        System.out.println("my2dArray3 row zero, column one= "+ my2dArray3[0][1]); //3
        System.out.println("my2dArray3 row zero, column two= "+ my2dArray3[0][2]); //4

        System.out.println("my2dArray3 row 1, column zero= "+ my2dArray3[1][0]); //5
        System.out.println("my2dArray3 row 1, column one= "+ my2dArray3[1][1]); //6
        System.out.println("my2dArray3 row 1, column two= "+ my2dArray3[1][2]); //7
        System.out.println("my2dArray3 row 1, column three= "+ my2dArray3[1][3]);//8
        //System.out.println("my2dArray3 row 1, column three= "+ my2dArray3[1][4]); //out of bound


        System.out.println("****************my3dArray1 *****************************");
        int [][][] my3dArray1 = new int [3][3][3];
        // first row
        my3dArray1[0][0][0] = 1;
        my3dArray1[0][0][1] = 2;
        my3dArray1[0][0][2] = 3;

        my3dArray1[0][1][0] = 4;
        my3dArray1[0][1][1] = 5;
        my3dArray1[0][1][2] = 6;

        my3dArray1[0][2][0] = 7;
        my3dArray1[0][2][1] = 8;
        my3dArray1[0][2][2] = 9;

        //second row
        my3dArray1[1][0][0] = 10;
        my3dArray1[1][0][1] = 11;
        my3dArray1[1][0][2] = 12;

        my3dArray1[1][1][0] = 13;
        my3dArray1[1][1][1] = 14;
        my3dArray1[1][1][2] = 15;

        my3dArray1[1][2][0] = 16;
        my3dArray1[1][2][1] = 17;
        my3dArray1[1][2][2] = 18;

        //third row
        my3dArray1[2][0][0] = 19;
        my3dArray1[2][0][1] = 20;
        my3dArray1[2][0][2] = 21;

        my3dArray1[2][1][0] = 22;
        my3dArray1[2][1][1] = 23;
        my3dArray1[2][1][2] = 24;

        my3dArray1[2][2][0] = 25;
        my3dArray1[2][2][1] = 26;
        my3dArray1[2][2][2] = 27;

        System.out.println("my3dArray1 third second zero= "+my3dArray1[2][1][0]);//22

    }
}
