package LearningJava.DheeruMundluru;

public class Method
{
    int instanceVariable = 5;
    static int staticVariable =2;



    public static void main(String[] args)
    {
        // method return type
        // The method return type is same as parameters type 'double', so no compiling error
        //sum(2.3,3.2);

        // the method return type 'double' is bigger than parameters type 'float', so no compiling error
        //sum_2(2.3f,(float)3.2);

        //the method return type 'float' is smaller than parameters type 'double', so compiling error. To solve we need to explicitly caste the parameters type to float
        //sum_3(2.5,3.4);


    }

    static double sum(double x, double y)
    {
        System.out.println("The method return type is same as parameters type 'double', so no compiling error ");
        return x+y;
    }

    // type has to match
    static double sum_2(float x, float y)
    {
        System.out.println("the method return type 'double' is bigger than parameters type 'float', so no compiling error ");
        return x+y;  //can return double from float
    }


    static float sum_3(double x, double y)
    {
        // this will give compiling error as we are trying to return float from double
        System.out.println("the method return type 'float' is smaller than parameters type 'double', so compiling error. To solve we need to explicitly caste the parameters type to float ");
        //return x+y; //can not return double from float
        return (float) (x+y); // we ca caste to float
    }

    public void instanceMethod()
    {
        System.out.println("this 'instanceMethod' method can access to the variable 'instanceVariable' "+ instanceVariable);
        System.out.println("this 'instanceMethod' method can access to the variable 'instanceVariable' "+ instanceVariable);

    }

    public static void staticMethods()
    {


    }


}
