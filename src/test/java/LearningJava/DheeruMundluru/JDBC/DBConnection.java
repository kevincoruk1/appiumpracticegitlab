package LearningJava.DheeruMundluru.JDBC;

import java.sql.*;

public class DBConnection
{
    public static void main(String[] args) throws SQLException
    {
        myDBConnection();
    }

/*
    install the JDBC API
    1- download the postgrsql jar from here:https://jdbc.postgresql.org/download.html
    2- save it some resource folder ( you need to keep the jar file saved there )
    3- Navigate to File>>>Project Structure>>>Libraries
    4- Click on + sign and select java
    5- Locate the jar file downloaded and click on open
    6- Click on “Apply”
 */


    public static void myDBConnection() throws SQLException
    {
//           // Connection method #1
//        String url = "jdbc:postgresql://srms-qa-loadtest-rds.cveuijr4mjrq.us-east-1.rds.amazonaws.com/srmsvnqa";
//        Properties props = new Properties();
//        props.setProperty("user","vnqadbapprole");
//        props.setProperty("password","o8143izyT");
//        //props.setProperty("ssl","true");
//        Connection conn = DriverManager.getConnection(url, props);


        //Connection method #2
        //String url = "jdbc:postgresql://host/databaseName?user=username&password=paswordhere";
        String url = "jdbc:postgresql://srms-qa-loadtest-rds.cveuijr4mjrq.us-east-1.rds.amazonaws.com/srmsvnqa?user=vnqadbapprole&password=o8143izyT";
        Connection conn = DriverManager.getConnection(url);

//          //Connection method #3
//          //String url = "jdbc:postgresql://host/databaseName";
//          String url = "jdbc:postgresql://srms-qa-loadtest-rds.cveuijr4mjrq.us-east-1.rds.amazonaws.com/srmsvnqa";
//          Connection conn = DriverManager.getConnection(url, "vnqadbapprole", "o8143izyT");


        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from vnus00020.locations where location_id <5;");
        resultSet.next();
        System.out.println("the location id for this location is " + resultSet.getString("location_id"));
        while (resultSet.next())
        {
            int location_id = resultSet.getInt("location_id");
            String barcode = resultSet.getString("barcode");
            String description = resultSet.getString("description");
            String name = resultSet.getString("name");

            System.out.println("location id " + location_id);
            System.out.println("location barcode " + barcode);
            System.out.println("location description " + description);
            System.out.println("location name " + name);


        }


    }
}
