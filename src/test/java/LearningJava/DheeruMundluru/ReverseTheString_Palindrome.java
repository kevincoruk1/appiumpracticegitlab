package LearningJava.DheeruMundluru;

public class ReverseTheString_Palindrome
{
    public static void main(String[] args)
    {
        String s1 = "madam";
        String reverseString = "";

        for (int i=s1.length()-1; i>=0; i--)
        {
            reverseString= reverseString + s1.charAt(i);
        }
        System.out.println("reverse of "+s1+ " is "+reverseString);

        // now you can even check if the string is palindrome or not (meaning the reverse of the string is same as the string itself)

        System.out.println("s1 is "+s1);
        System.out.println("reverse is "+s1);
        if (s1.equals(reverseString))
        {
            System.out.println("Yes this string is palindrome, meaning reverse of the string can be read as the string itself");
        }
        else
        {
            System.out.println("No its not palindrome ");
        }

    }

}
