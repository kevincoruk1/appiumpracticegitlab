package LearningJava.DheeruMundluru;

public class StringStaement
{
    /*

    https://docs.oracle.com/javase/8/docs/api/
    Click on java.lang under packages
    Click on ‘String’ class to learn more about String class

     */


    public static void main(String[] args)
    {
       // string();
       // stringBuilder();
        escapeSequences();

    }

    public static void string()
    {
        String s1 = "Hello world!";
        String s2 = "hello world!";
        String s3 = "  hello world!  ";


        System.out.println("the length of the string is =  --->> "+ s1.length()); //12
        System.out.println("is the string 's1' empty? = --->> "+ s1.isEmpty()); // false

        // compare
        System.out.println("is string 's1' = to \"hello world!\"? --->> "+ s1.equals("hello world!")); // false
        System.out.println("is string 's1' = to \"hello world!\"? (ignore the case) --->> "+ s1.equalsIgnoreCase("hello world!")); // true
        System.out.println("compareTo; will return zero if its identical,   = --->> "+ s1.compareTo("Hello world!")); // false

        // search
        System.out.println("does the string 's1' contains this word ? = --->> "+ s1.contains("ello")); // true
        System.out.println("does the string 's1' start with this ? = --->> "+ s1.startsWith("H")); // true
        System.out.println("does the string 's1' ends with this ? = --->> "+ s1.endsWith("x")); // false
        System.out.println("what is the index of this in 's1' string ? = --->> "+ s1.indexOf("w")); // 6
        System.out.println("what is the last index of this in 's1' string? = --->> "+ s1.lastIndexOf("l")); // 9


        System.out.println("what is the char at index 4 in 's1' string? = --->> "+ s1.charAt(4)); // o
        System.out.println("what is the string after 4th index in 's1' string? (included the 4th index) = --->> "+ s1.substring(4)); // o world!
        System.out.println("what is the substring between 4th index and 8 's1' string ?(included the 4th index) but excluded 8) = --->> "+ s1.substring(4,8)); // o wo

        System.out.println("convert 's1' string to lowercase = --->> "+ s1.toLowerCase()); //
        System.out.println("convert 'ssss2222' string to uppercase = --->> "+ s2.toUpperCase()); //


        System.out.println("trim 's3' string = --->> "+ s3.trim()); //
        System.out.println("replace o with x in 's1' string  = --->> "+ s1.replace("o","x")); //
        System.out.println("String.valueOf = --->> "+ String.valueOf(1.3)); //

        String [] sa = s1.split("o");
        {
            for (String temp : sa)
            {
                System.out.println("This will split the string based on the '0' char --->>> "+temp);
            }
        }
    }

    public static void stringBuilder()
    {
        String s = "hello "+ " world!";

        System.out.println("The string 's' is = "+s);
        StringBuilder sb = new StringBuilder (s);

        sb.append(" good").append(" morning :)");
        System.out.println("sb:--->>> "+ sb);
        System.out.println("this is appending the string 's' with word 'good' and 'morning' sb --->>> "+sb.toString());
        System.out.println("the length of the string is --->>> "+sb.length());

        sb.delete(1,5);
        System.out.println("This will delete the index from 1 (included) to 5(excluded) sb:--->>> "+ sb);

        sb.insert(1,"ey");
        System.out.println("This will insert the string after index 1  sb:--->>> "+ sb);


    }

    public static void escapeSequences()
    {
        String s1 = "Hello \"MR.\" John, How Are You?";
        System.out.println(" Double quote around \"MR.\" --->> "+s1 );

        char [] s2 = {'\'','e','l','l','o'};
        System.out.println(" Single quote around char [0] \' --->> "+s2[0] );

        String s3 = "Hello Mr. John, \nHow Are You?";
        System.out.println(" Start in a new line \n --->> "+s3 );

        String s4 = "Hello Mr. John, \n\tHow Are You?";
        System.out.println(" Start in a new line and horizantal tap \n\t --->> "+s4 );

        String s5 = "Hello Mr. John, How Are You\\?";
        System.out.println(" Insert backslash \\ --->> "+s5 );

        String s6 = "Hello Mr. John, How Are You?";
        System.out.println(" Double quote around \"MR.\" --->> "+s1 );
        String s7 = "Hello Mr. John, How Are You?";
        System.out.println(" Double quote around \"MR.\" --->> "+s1 );
        String s8 = "Hello Mr. John, How Are You?";
        String s9 = "Hello Mr. John, How Are You?";



    }

}
