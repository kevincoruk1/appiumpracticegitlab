package LearningJava.DheeruMundluru;

public class Casting
{

    public static void main(String[] args)
    {
        //Explicit casting
        long longValue = Long.MAX_VALUE;
        int intValue = Integer.MAX_VALUE;
        long x = 42;
        int y = (int) x;
        System.out.println("The value of long is "+longValue);
        System.out.println("The value of intValue is "+intValue);
        System.out.println("The value of y is "+y);


        byte bytevalue = Byte.MAX_VALUE;
        byte x1 = (byte)1234567;
        System.out.println("The value of bytevalue is "+bytevalue);
        System.out.println("The value of x1 is "+x1);

    }

}
