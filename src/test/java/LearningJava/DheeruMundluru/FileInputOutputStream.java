package LearningJava.DheeruMundluru;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;

public class FileInputOutputStream
{
    static String fileDirectory = System.getProperty("user.dir") + "/src/test/java/org/example/DheeruMundluru/Data/";

    public static void main(String[] args) throws IOException
    {

        // workingWithFile();
        // BufferedWriter("BufferedWriter_1");
        // BufferedReader("BufferedReader_1");

       // System.out.println("row 2 column 4 =" + getValueByIndexFromExcel(2, 4));
       //  readExceldata();

        excelWriterExample();
    }

    public static void workingWithFile() throws IOException
    {
        String filePath = fileDirectory + "File_2.txt";

        File myFile = new File(filePath);


        // System.out.println("creates a new file in 'filePath' (if its not already created)"+ myFile.createNewFile());
        // System.out.println("rename the file in 'myFile'e to 'renameit' if it exist -->> "+ myFile.renameTo(new File(fileDirectory+"renameit")));
        System.out.println("get getAbsolutePath -->> " + myFile.getAbsolutePath());
        System.out.println("delete the 'myFile' -->> " + myFile.delete());
        System.out.println("separator -->> " + myFile.separator);
        System.out.println("get getParent -->> " + myFile.getParent());
        System.out.println("get getAbsolutePath -->> " + myFile.getAbsolutePath());
        System.out.println("lastModified at -->> " + myFile.lastModified());
        System.out.println("exists -->> " + myFile.exists());
        System.out.println("isFile -->> " + myFile.isFile());
        System.out.println("isDirectory -->> " + myFile.isDirectory());
        System.out.println("length -->> " + myFile.length());

        // System.out.println("create a new directory on project level ..udemy/java/Folder1 -->> "+ new File("Folder1").mkdir());
        // System.out.println("delete the directory (make sure inside folder is empty)-->> "+ new File("Folder1").delete());

        File f2 = new File(fileDirectory + "File222");
        f2.createNewFile();
        f2.delete();

    }

    public static void BufferedReader(String fileName)
    {
        String filePath = fileDirectory + fileName + ".txt";

        System.out.println("File Path is " + filePath);

        StringBuilder text = new StringBuilder();

        try (BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(filePath))))
        {
            String line;
            while ((line = in.readLine()) != null)
            {
                text.append(line).append("\n");
                System.out.println("Reading from " + fileName + " --->>> " + line);
            }
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static void BufferedWriter(String fileName)
    {
        String filePath = fileDirectory + fileName + ".txt";

        System.out.println("File Path is " + filePath);

        StringBuilder text = new StringBuilder();

        try (BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath))))
        {
            out.write("hello there ");
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static String getValueByIndexFromExcel(int rowIndex, int colIndex)
    {
        FileInputStream file = null;
        try
        {
            file = new FileInputStream(new File(fileDirectory + "/Appium_VN_Barcodes.xlsx"));
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        XSSFWorkbook workbook = null;
        try
        {
            workbook = new XSSFWorkbook(file);
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        XSSFSheet sheet = workbook.getSheet("Product");
        Row rowNo = sheet.getRow(rowIndex - 1);
        String value = rowNo.getCell(colIndex - 1).toString();

        return value;
    }

    public static void readExceldata() // this is with method 'getValueByIndexFromExcel(int rowIndex, int colIndex)'
    {
        // 'readExceldata' method was part of 'getValueByIndexFromExcel' method but i am not sure what exactly it does
        // because 'getValueByIndexFromExcel' method can work alone
        FileInputStream file = null;
        try
        {
            file = new FileInputStream(new File(fileDirectory + "/Appium_VN_Barcodes.xlsx"));
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        XSSFWorkbook workbook = null;
        try
        {
            workbook = new XSSFWorkbook(file);
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }

        XSSFSheet sheet = workbook.getSheet("Product");
    /*
    //Iterate through each rows one by one
    Iterator<Row> rowIterator = sheet.iterator();
    while (rowIterator.hasNext())
    {
        Row row = rowIterator.next();
        Iterator<Cell> cellIterator = row.cellIterator();

        while (cellIterator.hasNext())
        {
            Cell cell = cellIterator.next();
            //Check the cell type and format accordingly
            switch (cell.getCellType())
            {
                case Cell.CELL_TYPE_NUMERIC:
                    System.out.print(cell.getNumericCellValue() + "\t");
                    break;
                case Cell.CELL_TYPE_STRING:
                    System.out.print(cell.getStringCellValue() + "\t");
                    break;
            }
        }
        System.out.println("");
    }
    */
        try
        {
            file.close();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }


    public static void excelWriterExample() throws IOException
    {

            XSSFWorkbook workbook = new XSSFWorkbook();
            // this will create the excel file on user directory level: /Users/kcoruk/Kevin/Automation/Projects/Udemy/Java/excel1.xlsx
            XSSFSheet sheet = workbook.createSheet("excel1");


            Object[][] bookData =
            {
                    {"Head First Java", "Kathy Serria", 79},
                    {"Effective Java", "Joshua Bloch", 36},
                    {"Clean Code", "Robert martin", 42},
                    {"Thinking in Java", "Bruce Eckel", 35},
            };

            int rowCount = 0;

            for (Object[] aBook : bookData)
            {
                Row row = sheet.createRow(++rowCount);

                int columnCount = 0;

                for (Object field : aBook)
                {
                    Cell cell = row.createCell(++columnCount);
                    if (field instanceof String)
                    {
                        cell.setCellValue((String) field);
                    }
                    else if (field instanceof Integer)
                    {
                        cell.setCellValue((Integer) field);
                    }
                }

            }


            try (FileOutputStream outputStream = new FileOutputStream("excel1.xlsx"))
            {
                workbook.write(outputStream);
            }
    }
}

