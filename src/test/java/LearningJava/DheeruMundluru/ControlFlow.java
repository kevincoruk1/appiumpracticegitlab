package LearningJava.DheeruMundluru;

public class ControlFlow
{

    public static void main(String[] args)
    {
        // ternaryOperator();
        // ifStatement();
        // forEachStatement();
        // forStatement_1();
        // forStatement_2();
        // forStatement_3();
        // forStatement_4();
        // whileLoop();
        // doWhileLoop();
        // _break();
        _continue();


    }


    public static void ternaryOperator()
    {
        //Ternary Operator
        //result = (booleanValue)? trueExpression : falseExpression;

        {
            int result = 0;
            int x = 4;
            int y = 4;

            result = (x < y) ? x : y;

            System.out.println("ternary statement result = " + result);
        }
    }


    public static void ifStatement()
    {
        int age = 27;
        int salary = 60000;
        boolean hasBadCredit = false;
        boolean approved = false;

        if (age >= 25 && age <= 35 && salary >= 50000)
        {
            approved = true;
            System.out.println("Credit approved for condition age>= 25 && age<=35 && salary >=50000 ");
        } else if (age >= 36 && age <= 45 && salary >= 70000)
        {
            approved = true;
            System.out.println("Credit approved for condition age>= 36 && age<=45 && salary >=70000 ");

        } else
        {
            if (age >= 55 && hasBadCredit == true)
            {
                approved = false;
                System.out.println("Credit approved for condition age>= 55 && hasBadCredit == true ");
            }
        }
    }

    public static void forEachStatement()
    {

        int[] dice1 = {1, 2, 3, 4, 5, 6};
        int[] dice2 = {1, 2, 3, 4, 5, 6};

        for (int i : dice1)
        {
            for (int j : dice2)
            {
                System.out.println("dice  " + i + " of  " + j);
            }

        }
    }

    public static void forStatement_1()
    {
        // use for loop if you know the numbers of iterations, otherwise use while loop
        int[] myArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        for (int i = 0; i < myArray.length; i++)
        {
            System.out.println("method for_1 " + (myArray[i]));
            if (i==5)
            {
                System.out.println("This will exit the for loop once if condition is met");
                break;
            }

        }
        // can be expressed like this
        for (int i = 0; i < myArray.length; System.out.println("this is acceptable " + myArray[i]), i++) ;
        // can be expressed like this as well
        for (int i = 0; i < myArray.length; System.out.println("this is acceptable as well " + myArray[i++])) ;

    }

    public static void forStatement_2()
    {

        int[] myArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        for (int i = 0, j = 0; i < myArray.length && j < myArray.length; i++, j += 2)
        {
            System.out.println("myArray[i] = " + (myArray[i]) + " myArray[j] = " + myArray[j]);
        }


    }

    public static void forStatement_3()
    {

        int[] myArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        for (int i = 0, j = myArray.length - 1, middle = myArray.length / 2; i < middle; i++, j--)
        {
            int temp = myArray[i];
            myArray[i] = myArray[j];
            myArray[j] = temp;
        }
        for (int i = 0; i < myArray.length; i++)
        {
            System.out.println(" Reverse of myArray[i] " + myArray[i]);

        }
    }

    public static void forStatement_4()
    {

        int[][] studentGrades = {{60, 80, 90}, {90, 80, 70}, {50, 80, 70}};
        int max = 0;
        for (int i = 0; i < studentGrades.length; i++)
        {
            //System.out.println("Displaying the grades of section " +i);
            for (int j = 0; j < studentGrades[i].length; j++)
            {
                if (studentGrades[i][j] > max)
                {
                    max = studentGrades[i][j];
                }
                System.out.println("Student grades for section " + i + " = " + studentGrades[i][j]);
            }
            System.out.println("Max Student grades for section " + i + " = " + max);
        }
    }

    public static void whileLoop()
    {
        // use for loop if you know the numbers of iterations, otherwise use while loop
        int x = 0;
        while (x < 5)
        {
            System.out.println("while loop; it will first check if the condition is met.");
            x++;
        }

        /*
        // real use case of while loop
        while (true)
        {
            if (getTime()==8)
            {
                sendEmails();
            }
        }

        //same can be achived by for loop
        for (; ;)
        {
            if (getTime()==8)
            {
                sendEmails();
            }

        }
        */
    }

    public static void doWhileLoop()
    {
        //
        int x = 0;
        do
        {
            System.out.println("Do while loop; it will first execute then check if the condition is met.");
            x++;
        }
        while (x< 5);
    }

    public static void _break()
    {
        break_A_Loop : for (int a = 0; a <= 5; a++)
        {
            System.out.println("the value for A is " + a);
            break_B_Loop : for (int b = 0; b <= 5; b++)
            {
                System.out.println("the value for B is " + b);
                break_C_Loop : for (int c = 0; c <= 5; c++)
                {
                    System.out.println("the value for C is " + c);
                    if (c == 2)
                    {
                        System.out.println("Break; the value for C is now = 2--->>" + c);
                        // this will break the 'A'loop. The c loop will run 2 times
                        // break break_A_Loop;

                        // this will break the 'B'loop. The A loop will run 5 times
                        // break break_B_Loop;

                        // this will break the 'C'loop
                        break;  //break_C_Loop;
                    }
                }
            }
        }
    }

    public static void _continue()
    {

        continue_A : for (int a = 0; a <= 5; a++)
        {
            System.out.println("the value for A is " + a);
            continue_B : for (int b = 0; b <= 5; b++)
            {
                System.out.println("the value for B is " + b);
                continue_C : for (int c = 0; c <= 5; c++)
                {
                    System.out.println("the value for C is " + c);
                    if (c == 2)
                    {
                        System.out.println("Break; the value for C is now = 2--->>" + c);
                        // once the 'c == 2' condition is met, it will exit the inner loop for c and continue outer loop A
                         continue continue_A;

                        // once the 'c == 2' condition is met, it will exit the inner loop for c and continue outer loop B
                       // continue continue_B;

                    }
                }
            }
        }
    }
}



