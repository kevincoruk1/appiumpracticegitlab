package LearningJava.DheeruMundluru;

public class Recursion
{
    public static void main(String[] args)
    {
        // factorial_Non_Recursion (1);
       // factorial(4); // this one is not working

        // in this array of 'a', start from index of '0', maximum index of 9, look for '5'
        binarySearch((new int [] {0,1,2,5,6,7,8,9,10}),0,9,5);
    }

    public static int factorial_Non_Recursion(int n)
    {
        if (n==0 || n==1)
        {
            System.out.println("the "+n+ " factorial = "+1);
            return 1;
        }else
        {
            int factorial = n;
            while (n>=2)
            {
                factorial = factorial * (n-1);
                n--;
            }
            System.out.println("the factorial = "+factorial);
            return  factorial;
        }
    }

    public static int factorial(int m)
    {
        if (m == 0)
        {
            System.out.println("the factorial = "+1);
            return 1;
        }
        System.out.println("the value of m before last return is "+m);
        return m * factorial(m-1);

    }


    public static int  binarySearch(int a[],int low, int high, int searchKey)
    {
        if (low == high)
        {
            if (searchKey == a[low])
            {
                System.out.println("the index is "+low);
                return low;
            }
            else
            {
                System.out.println("cant find the element returning -1");
                return -1;
            }
        }
        else
        {
            int mid = (low + high) / 2;
            if (searchKey == a[mid])
            {
                return mid;
            } else if (searchKey > a[mid])
            {
                return binarySearch(a, mid + 1, high, searchKey);
            } else
            {
                return binarySearch(a, low, mid - 1, searchKey);
            }
        }
    }

}

