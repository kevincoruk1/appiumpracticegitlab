package LearningJava.DheeruMundluru;

public class ArictmetricOperators
{

    public static void main(String[] args)
    {
        post();
        pre();

        mathOperation();

        oddOrEven(50);
    }

    public static void post()
    {
        // post
        int x = 5;
        int y = 0;
        y= x++; // it will first assign y to x then add 1 to x
        System.out.println("it will first assign y to x then add 1 to x so y = "+y); //5
        System.out.println("it will first assign y to x then add 1 to x so x = "+x); //6
    }

    public static void pre()
    {
        // pre
        int x = 5;
        int y = 0;
        y= ++x;  // it will first add 1 to x then assign to y
        System.out.println("it will first add 1 to x then assign to y so y = "+y); //6
        System.out.println("it will first add 1 to x then assign to y so x = "+x); //6
    }

    public static void mathOperation()
    {
        int x = 100;
        System.out.println(" x -= 5 = 5  --->>> "+ ( x -=5 ) ); // x= 95 (100-5)
        System.out.println(" x += 5 = 10 --->>> "+ ( x +=5 ) ); // x = 100 (95+5)
        System.out.println(" x /= 5 = 2  --->>> "+ ( x /=5 ) ); // x = 20 (100/5)
        System.out.println(" x *= 5 = 50 --->>> "+ ( x *=5 ) ); // x = 100 (20*100)

        System.out.println("But  x =- 5 = -5 --->>> "+ ( x =- 5 ) );
        System.out.println("But  x =+ 5 = +5 --->>> "+ ( x =+ 5 ) );
        // The below lines will give you compiling error
        //System.out.println(" x /= 5 = 2  --->>> "+ ( x =/ 5 )) ;
        //System.out.println(" x *= 5 = 50 --->>> "+ ( x =* 5 )) ;

    }

    public static void oddOrEven(int number)
    {
        int  oddOrEven = number % 2;
        if (oddOrEven > 0)
        {
            System.out.println("The number "+number +" is an odd number");
        } else
        {
            System.out.println("The number "+number +" is an even number");
        }


    }

}
