package LearningJava.DheeruMundluru;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Date_And_Calendar
{
    public static void main(String[] args)
    {
        Date d = new Date();
        System.out.println("the date without the format is "+d.toString());
        SimpleDateFormat sdf1 = new SimpleDateFormat("G");
        SimpleDateFormat sdf2 = new SimpleDateFormat("y");
        SimpleDateFormat sdf3 = new SimpleDateFormat("yy");
        SimpleDateFormat sdf4 = new SimpleDateFormat("M/d/yyy");
        SimpleDateFormat sdf5 = new SimpleDateFormat("h");

        System.out.println("the date format #1 is "+sdf1.format(d));
        System.out.println("the date format #2 is "+sdf2.format(d));
        System.out.println("the date format #3 is "+sdf3.format(d));
        System.out.println("the date format #4 is "+sdf4.format(d));
        System.out.println("the date format #5 is "+sdf5.format(d));


        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf6 = new SimpleDateFormat("M/d/yyyy hh:mm:ss");
        System.out.println("Calendar time         "+sdf6.format(cal.getTime()));
        System.out.println("the date format #6 is "+sdf6.format(d));

        System.out.println("The day of the month is "+(cal.get(Calendar.DAY_OF_MONTH)));
        System.out.println("The week of the month is "+(cal.get(Calendar.WEEK_OF_MONTH)));

    }

}

/*

G	Era designator (before christ, after christ)
y	Year (e.g. 12 or 2012). Use either yy or yyyy.
M	Month in year. Number of M's determine length of format (e.g. MM, MMM or MMMMM)
d	Day in month. Number of d's determine length of format (e.g. d or dd)
h	Hour of day, 1-12 (AM / PM) (normally hh)
H	Hour of day, 0-23 (normally HH)
m	Minute in hour, 0-59 (normally mm)
s	Second in minute, 0-59 (normally ss)
S	Millisecond in second, 0-999 (normally SSS)
E	Day in week (e.g Monday, Tuesday etc.)
D	Day in year (1-366)
F	Day of week in month (e.g. 1st Thursday of December)
w	Week in year (1-53)
W	Week in month (0-5)
a	AM / PM marker
k	Hour in day (1-24, unlike HH's 0-23)
K	Hour in day, AM / PM (0-11)
z	Time Zone
'	Escape for text delimiter
'	Single quote
 */