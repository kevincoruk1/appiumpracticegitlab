package AndroidApiDemosDebug.TestsCases;

import org.testng.annotations.Test;

import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.appiumDriver;

public class General_Store_Create_User_And_Login
{

    @Test(priority = 1)
    public static void login()
    {
        System.out.println("login method executed ?");
        // click on select country drop down
        appiumDriver.findElementById("com.androidsample.generalstore:id/spinnerCountry").click();
        // select the country
        // if below line doesnt work with some iOS version use the other one
        appiumDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"Belarus\"))" ).click();
        //appiumDriver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textMatches(\"Belarus\").instance(0))")).click();
        // enter your name
        appiumDriver.findElementById("com.androidsample.generalstore:id/nameField").sendKeys("myName is Kev");
        //Select gender
        appiumDriver.findElementById("com.androidsample.generalstore:id/radioFemale").click();
        //click on lets shop button
        appiumDriver.findElementById("com.androidsample.generalstore:id/btnLetsShop").click();

        try
        {
            Thread.sleep(5000);
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
      //  Assert.assertEquals(appiumDriver.findElementById("com.androidsample.generalstore:id/toolbar_title").getText(), "Products");





    }


}
