package AndroidApiDemosDebug.TestsCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.appiumDriver;
import static AndroidApiDemosDebug.CommonLibrary.Long_Press.longPress;

public class Long_Press
{
    @BeforeClass
    public void testingBeforeClass()
    {
        System.out.println("This is before class");
    }

    @Test(priority = 1)
    public void testingtests()
    {

        /* Need to add here
        #1  TouchAction >>>Appium method
        #2  TouchAction >>> Android Method
        #3  TouchAction >>> Selenium Method
        single tap
        double tap
        long press
        short press etc.

         */

        appiumDriver.findElementByAndroidUIAutomator("text(\"Views\")").click();
        appiumDriver.findElementByAndroidUIAutomator("text(\"Expandable Lists\")").click();
        appiumDriver.findElementByAndroidUIAutomator("text(\"1. Custom Adapter\")").click();
        WebElement peopleNames = appiumDriver.findElementByAndroidUIAutomator("text(\"People Names\")");

        longPress(peopleNames);

        System.out.println("the text value is "+appiumDriver.findElementsByClassName("android.widget.TextView").get(0).getAttribute("text"));



    }



}
