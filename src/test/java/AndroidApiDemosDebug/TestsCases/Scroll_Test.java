package AndroidApiDemosDebug.TestsCases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.appiumDriver;
import static AndroidApiDemosDebug.CommonLibrary.Scroll.scrollToElement2;

public class Scroll_Test
{
    @BeforeClass
    public void testingBeforeClass()
    {
        System.out.println("This is before class");
    }

    @Test(priority = 1)
    public void testingtests()
    {
        // Android API Demos app
//        appiumDriver.findElementByAndroidUIAutomator("text(\"Views\")").click();
//        scrollToElement1("WebView").click();

        //General-Store.apk
        General_Store_Create_User_And_Login.login();
        scrollToElement2("com.androidsample.generalstore:id/rvProductList","Jordan 6 Rings",0);
        appiumDriver.findElementByAndroidUIAutomator("text(\"ADD TO CART\")").click();
        appiumDriver.findElementById("com.androidsample.generalstore:id/appbar_btn_cart").click();

        try
        {
            Thread.sleep(5000);
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }


    }
}
