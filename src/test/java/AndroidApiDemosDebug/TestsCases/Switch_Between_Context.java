package AndroidApiDemosDebug.TestsCases;

import java.util.Set;

import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.appiumDriver;

public class Switch_Between_Context
{

    public void switchContext()
    {
        System.out.println("The context before switching is "+appiumDriver.getContextHandles());
        Set<String> contextNames = appiumDriver.getContextHandles();

        for (String contextName : contextNames)
        {
            System.out.println("Print all contexts "+contextName); //prints out something like NATIVE_APP \n WEBVIEW_1
        }
        appiumDriver.context(String.valueOf(contextNames.toArray()[1])); // set context to WEBVIEW_1
        System.out.println("The context after switching to [1] Webview "+appiumDriver.getContextHandles());


        //do some web testing
        //appiumDriver.findElement(By.cssSelector(".green_button")).click();

        appiumDriver.context("NATIVE_APP");
        System.out.println("The context after switching to 'NATIVE_APP' "+appiumDriver.getContextHandles());

        // do more native testing if we want

       // appiumDriver.quit();
    }


}
