package AndroidApiDemosDebug.TestsCases;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.appiumDriver;
import static AndroidApiDemosDebug.CommonLibrary.Swipe.swipeToElement;

public class Swipe
{
    @BeforeClass
    public void testingBeforeClass()
    {
        System.out.println("This is before class");
    }

    @Test(priority = 1)
    public void testingtests()
    {

        appiumDriver.findElementByAndroidUIAutomator("text(\"Views\")").click();
        appiumDriver.findElementByAndroidUIAutomator("text(\"Date Widgets\")").click();
        appiumDriver.findElementByAccessibilityId("2. Inline").click();
        appiumDriver.findElementByAndroidUIAutomator("description(\"8\")").click();

        WebElement from = appiumDriver.findElementByAndroidUIAutomator("description(\"15\")");
        WebElement to = appiumDriver.findElementByAndroidUIAutomator("description(\"45\")");
        Point fromElement =appiumDriver.findElementByAndroidUIAutomator("description(\"15\")").getLocation();
        Point toElement = appiumDriver.findElementByAndroidUIAutomator("description(\"45\")").getLocation();

        swipeToElement(from, to);


        //TouchAction swipe = new TouchAction(getAppiumDriver());
        //swipe from one element to another by first getting the element's coordinates
        //swipe.press(PointOption.point(fromElement)).waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2))).moveTo(PointOption.point(toElement)).release().perform();
        // Swipe from one element to another
        //swipe.longPress(element(from)).moveTo(element(to)).release().perform();
        //swipe.longPress(element(from)).waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2))).moveTo(element(to)).release().perform();



    }



}
