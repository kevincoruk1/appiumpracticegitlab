package AndroidApiDemosDebug.TestsCases;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.offset.ElementOption;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.annotations.Test;

import java.util.Set;

import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.appiumDriver;
import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.getAppiumDriver;


public class General_Store_Add_To_Cart_Verify_TermsAndConditionLink
{
    static General_Store_Create_User_And_Login user = new General_Store_Create_User_And_Login();

    @Test(priority = 1)
    public static void login() throws InterruptedException
    {
        user.login();

        // #1 scroll find the "air jordan 6 rings"
        appiumDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector())" +
                ".scrollIntoView(text(\"Jordan 6 Rings\"))");

        // first item
        appiumDriver.findElements(By.id("com.androidsample.generalstore:id/productAddCart")).get(0).click();
        appiumDriver.findElements(By.id("com.androidsample.generalstore:id/productAddCart")).get(1).click();
        //click on the cart icon
        appiumDriver.findElementById("com.androidsample.generalstore:id/appbar_btn_cart").click();
        // tab on the check box
        AndroidElement element = appiumDriver.findElementByClassName("android.widget.CheckBox");
        System.out.println("the checkbo text is "+appiumDriver.findElementByClassName("android.widget.CheckBox").getText());
        TouchAction tap = new TouchAction(getAppiumDriver());
       // tap.tap(TapOptions.tapOptions().withElement(ElementOption.element(element))).perform();
        // or
        tap.tap(TapOptions.tapOptions().withElement(ElementOption.element(appiumDriver.findElementByClassName("android.widget.CheckBox")))).perform();


        // verify if checkbox is checked >>checked =true;
        System.out.println("is checkbox checked? "+appiumDriver.findElementByClassName("android.widget.CheckBox").getAttribute("checkable"));

        // long press on the T&C
        //AndroidElement tc = appiumDriver.findElementByAndroidUIAutomator("new UiSelector().text(\"Please read our terms of conditions\")");
//        AndroidElement tc = appiumDriver.findElementByXPath("//*[@text='Please read our terms of conditions']");
//        tap.longPress(LongPressOptions.longPressOptions().withElement(ElementOption.element(tc)))
//                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2))).release().perform();
       // get the T&C text
       // System.out.println("The T&C text is " +appiumDriver.findElementByClassName("android.widget.TextView").getText());

        // close the T&C window
        // appiumDriver.findElementByAndroidUIAutomator("text(\"CLOSE\")").click();

        System.out.println("#1 The app context BEFORE Navigate to google is now "+appiumDriver.getContext());
        // navigate to website
        appiumDriver.findElementByAndroidUIAutomator("new UiSelector().className(\"android.widget.Button\")").click();
        Thread.sleep(5000);

        // set the context to webview
        Set<String> context = appiumDriver.getContextHandles();
        for (String contextNames:context)
        {
            System.out.println("Contexts are : "+contextNames);
        }
        appiumDriver.context("WEBVIEW_com.androidsample.generalstore");
        //COULDNT MAKE IT WORK THOSE 2 LINES
        appiumDriver.findElementByName("q").sendKeys("I did it");
        appiumDriver.findElementByName("q").sendKeys(Keys.ENTER);

        // navigate back to app
        appiumDriver.pressKey(new KeyEvent(AndroidKey.BACK));
        // set the context to NATIVE_APP
        appiumDriver.context("NATIVE_APP");

    }

    public static void main(String[] args)
    {


    }

}
