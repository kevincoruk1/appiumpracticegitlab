package AndroidApiDemosDebug.TestsCases;

import io.appium.java_client.TouchAction;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;

import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.appiumDriver;
import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.getAppiumDriver;
import static AndroidApiDemosDebug.CommonLibrary.Take_Screenshot.screenshotOnFailure;
import static io.appium.java_client.touch.offset.ElementOption.element;

public class Take_Screenshot_On_Fail
{


    @BeforeClass
    public void testingBeforeClass()
    {
        System.out.println("This is before class");
    }

    @Test(priority = 1)
    public void takeScreenshot() throws InterruptedException, IOException
    {

        Thread.sleep(2000);
        screenshotOnFailure("HomePageScreen");
        Thread.sleep(2000);
        appiumDriver.findElementByAndroidUIAutomator("text(\"Views\")").click();
        Thread.sleep(2000);
        screenshotOnFailure("ViewsScreen");
        Thread.sleep(2000);
        appiumDriver.findElementByAndroidUIAutomator("text(\"Drag and Drop\")").click();
        Thread.sleep(2000);
        screenshotOnFailure("DragAndDropScreen");
        Thread.sleep(2000);
        WebElement elementFrom = appiumDriver.findElementsByAndroidUIAutomator("className(\"android.view.View\")").get(0);
        // setting the index to 2222 so it can fail
        WebElement elementTo = appiumDriver.findElementsByAndroidUIAutomator("className(\"android.view.View\")").get(2222);
        TouchAction action = new TouchAction(getAppiumDriver());
        action.longPress(element(elementFrom)).moveTo(element(elementTo)).release().perform();
        Thread.sleep(5000);
        System.out.println("the text SIZE is "+appiumDriver.findElementsByAndroidUIAutomator("className(\"android.widget.TextView\")").size());
        System.out.println("the text is displayed ? "+appiumDriver.findElementsByAndroidUIAutomator("className(\"android.widget.TextView\")").get(3).isDisplayed());
        System.out.println("the text is "+appiumDriver.findElementsByAndroidUIAutomator("className(\"android.widget.TextView\")").get(3).getText());




    }



}
