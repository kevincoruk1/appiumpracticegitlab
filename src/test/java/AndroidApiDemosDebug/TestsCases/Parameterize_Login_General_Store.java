package AndroidApiDemosDebug.TestsCases;

import AndroidApiDemosDebug.Resources.TestData_Parameterize;
import org.testng.Assert;
import org.testng.annotations.Test;

import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.appiumDriver;

public class Parameterize_Login_General_Store
{
   // receiving some error here, but there is a working sample of dataProvider in TestNG/Parameters_WithTestNG

    @Test(dataProvider ="InputData", dataProviderClass = TestData_Parameterize.class )
    public void login(String input) throws InterruptedException
    {
        System.out.println("login method executed ?");
        // click on select country drop down
        appiumDriver.findElementById("com.androidsample.generalstore:id/spinnerCountry").click();
        // select the country
        // if below line doesnt work with some iOS version use the other one
        appiumDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"Belarus\"))" ).click();
        //appiumDriver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textMatches(\"Belarus\").instance(0))")).click();
        // enter your name
        appiumDriver.findElementById("com.androidsample.generalstore:id/nameField").sendKeys(input);
        //Select gender
        appiumDriver.findElementById("com.androidsample.generalstore:id/radioFemale").click();
        //click on lets shop button
        appiumDriver.findElementById("com.androidsample.generalstore:id/btnLetsShop").click();
        Thread.sleep(5000);
        Assert.assertEquals(appiumDriver.findElementById("com.androidsample.generalstore:id/toolbar_title").getText(), "Products");





    }


}
