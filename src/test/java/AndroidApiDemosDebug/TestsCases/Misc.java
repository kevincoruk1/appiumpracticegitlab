package AndroidApiDemosDebug.TestsCases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.appiumDriver;

public class Misc
{
    @BeforeClass
    public void testingBeforeClass()
    {
        System.out.println("This is before class");
    }

    @Test(priority = 1)
    public void testingtests() throws InterruptedException
    {

        appiumDriver.findElementByAndroidUIAutomator("text(\"Views\")").click();
        appiumDriver.navigate().back();

        System.out.println("the currentactivity is "+appiumDriver.currentActivity());
        System.out.println("the context is "+appiumDriver.getContext());
        System.out.println("the platform is "+appiumDriver.getPlatformName());
        System.out.println("the orientiation is "+appiumDriver.getOrientation());
        System.out.println("the 'isDeviceLocked' "+appiumDriver.isDeviceLocked());
        System.out.println("the automationName is "+appiumDriver.getAutomationName());

    }



}
