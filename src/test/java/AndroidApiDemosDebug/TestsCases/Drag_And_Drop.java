package AndroidApiDemosDebug.TestsCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.appiumDriver;
import static AndroidApiDemosDebug.CommonLibrary.Drag_And_Drop.dragAndDrop;

public class Drag_And_Drop
{
    @BeforeClass
    public void testingBeforeClass()
    {
        System.out.println("This is before class");
    }

    @Test(priority = 1)
    public void testingtests() throws InterruptedException
    {

        appiumDriver.findElementByAndroidUIAutomator("text(\"Views\")").click();
        appiumDriver.findElementByAndroidUIAutomator("text(\"Drag and Drop\")").click();
        WebElement from = appiumDriver.findElementsByAndroidUIAutomator("className(\"android.view.View\")").get(0);
        WebElement to = appiumDriver.findElementsByAndroidUIAutomator("className(\"android.view.View\")").get(2);

        dragAndDrop(from, to);

        Thread.sleep(5000);
        System.out.println("the text SIZE is "+appiumDriver.findElementsByAndroidUIAutomator("className(\"android.widget.TextView\")").size());
        System.out.println("the text is displayed ? "+appiumDriver.findElementsByAndroidUIAutomator("className(\"android.widget.TextView\")").get(3).isDisplayed());
        System.out.println("the text is "+appiumDriver.findElementsByAndroidUIAutomator("className(\"android.widget.TextView\")").get(3).getText());




    }



}
