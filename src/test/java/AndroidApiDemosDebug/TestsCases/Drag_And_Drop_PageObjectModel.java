package AndroidApiDemosDebug.TestsCases;

import AndroidApiDemosDebug.ObjectRepository.Drag_And_Drop;
import AndroidApiDemosDebug.ObjectRepository.HomePage;
import AndroidApiDemosDebug.ObjectRepository.Views;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.appiumDriver;
import static AndroidApiDemosDebug.CommonLibrary.Drag_And_Drop.dragAndDrop;

public class Drag_And_Drop_PageObjectModel
{
    @BeforeClass
    public void testingBeforeClass()
    {
        System.out.println("This is before class");
    }

    @Test(priority = 1)
    public void dargAndDropPOM() throws InterruptedException
    {
        HomePage homePage = new HomePage(appiumDriver);
        Views views = new Views(appiumDriver);
        Drag_And_Drop drag_and_drop = new Drag_And_Drop(appiumDriver);

        // #1
        //homePage.views1.click();
        // #2
        homePage.views2().click();

        // #1
        //views.Drag_And_Drop.click();
        // #2
        views.getDragAndDrop().click();

        WebElement from = drag_and_drop.getElements().get(0);
        WebElement to = drag_and_drop.getElements().get(2);

        // calling drag and drop method
        dragAndDrop(from, to);



    }



}
