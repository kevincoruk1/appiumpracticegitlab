package AndroidApiDemosDebug.TestsCases;

import org.testng.annotations.*;

import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.*;

public class Before_And_After_Suite
{

    @BeforeSuite
    public static void startAppiumServerAndDriverBeforeSuite()
    {
        startAppiumDriver();
        System.out.println("Appium Server started");
    }


    @AfterSuite
    public static void stopAppiumServerAndDriverAfterSuite()
    {
        stopAppiumDriver();
        System.out.println("Appium Server stopped");
    }

//    @BeforeMethod
//    public void startApplication()
//    {
//        System.out.println("start app method called");
//        appiumDriver.launchApp();
//    }
//    @AfterTest
//    public void stopApplication()
//    {
//        System.out.println("stop app method called");
//        appiumDriver.closeApp();
//    }




}
