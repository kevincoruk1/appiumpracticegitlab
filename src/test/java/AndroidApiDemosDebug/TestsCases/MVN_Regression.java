package AndroidApiDemosDebug.TestsCases;

import org.testng.annotations.Test;

public class MVN_Regression
{
    public static void main(String[] args)
    {
        mvnCmdTest();
    }
    @Test
    public static void mvnCmdTest()
    {
        System.out.println
                (" This is to run REGRESSION TC cd /Users/Kevin/Documents/Automation/Gitlab/ && mvn test -PRegression" +
                "This is running from gitlab. This is to run REGRESSION TC cd /Users/Kevin/Documents/Automation/Gitlab/ && mvn test -PRegression" +
                "appiumpracticegitlab This is to run REGRESSION TC cd /Users/Kevin/Documents/Automation/Gitlab/ && mvn test -PRegression" +
                "<profiles>\n" +
                "    <profile>\n" +
                "        <id>Regression</id>\n" +
                "            <build>\n" +
                "                <plugin>\n" +
                "                    <groupId>org.apache.maven.plugins</groupId>\n" +
                "                    <artifactId>maven-surefire-plugin</artifactId>\n" +
                "                    <version>3.0.0-M4</version>\n" +
                "                    <configuration>\n" +
                "                        <suiteXmlFiles>\n" +
                "                            <suiteXmlFile>src/test/java/AndroidApiDemosDebug/TestNGXML/TestNG_Regression.xml\n" +
                "                            </suiteXmlFile>\n" +
                "                        </suiteXmlFiles>\n" +
                "                    </configuration>\n" +
                "                </plugin>\n" +
                "            </build>\n" +
                "    </profile>\n" +
                "    <profile>\n" +
                "        <id>Smoke</id>\n" +
                "            <build>\n" +
                "                <plugin>\n" +
                "                    <groupId>org.apache.maven.plugins</groupId>\n" +
                "                    <artifactId>maven-surefire-plugin</artifactId>\n" +
                "                    <version>3.0.0-M4</version>\n" +
                "                    <configuration>\n" +
                "                        <suiteXmlFiles>\n" +
                "                            <suiteXmlFile>src/test/java/AndroidApiDemosDebug/TestNGXML/TestNG_Smoke.xml\n" +
                "                            </suiteXmlFile>\n" +
                "                        </suiteXmlFiles>\n" +
                "                    </configuration>\n" +
                "                </plugin>\n" +
                "            </build>\n" +
                "    </profile>\n" +
                "</profiles>");
    }
}
