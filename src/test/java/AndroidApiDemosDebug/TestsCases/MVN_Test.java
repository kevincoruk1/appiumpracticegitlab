package AndroidApiDemosDebug.TestsCases;

import org.testng.annotations.Test;

public class MVN_Test
{
    public static void main(String[] args)
    {
        mvnCmdTest();
    }
    @Test
    public static void mvnCmdTest()
    {
        System.out.println("This is to run TEST TC without testng xml file on cmd 'mvn test cmd' ");

        System.out.println
                ("To run the test cases with 'mvn test' cmd (without 'TestNG.xml' file)" +'\n'+
                "1- Make sure you use 'Test' suffix for your testname. If your test case name is payment, it should be like 'paymentTest' " +'\n'+
                "2- Go to your terminal and paste this <cd your/pom/file/path && mvn test>"+'\n' +
                "example: cd /Users/kcoruk/Documents/Automation/AppiumPractice/ && mvn test\"" +'\n'+
                "If you set up the below steps in POM file, 'mvn test command only runs the TC in the xml file not other "+'\n'+
                "To run your 'TestNG.xml' file through terminal using 'mvn test'" +'\n'+
                "1- Make sure you use 'Test' suffix for your testname. If your test case name is payment, it should be like 'paymentTest' " +'\n'+
                "2- Make sure the testcase is added to your testNG.xml file" +'\n'+
                "3- Navigate to your project 'POM' file and locate 'maven-surefire-plugin' and set the 'testNG.xml' file path in 'suiteXmlFile'" +'\n'+
                "                <plugin>\n" +
                "                    <groupId>org.apache.maven.plugins</groupId>\n" +
                "                    <artifactId>maven-surefire-plugin</artifactId>\n" +
                "                    <version>3.0.0-M4</version>\n" +
                "                    <configuration>\n" +
                "                        <suiteXmlFiles>\n" +
                "                            <suiteXmlFile>src/test/java/AndroidApiDemosDebug/TestNGXML/TestNG.xml</suiteXmlFile>\n" +
                "                        </suiteXmlFiles>\n" +
                "                    </configuration>\n" +
                "                </plugin>\n"+
                "4- Get your 'POM' file path" +'\n'+
                "5- Go to terminal and paste this <cd your/pom/file/path && mvn test>" +'\n'+
                "example: cd /Users/kcoruk/Documents/Automation/AppiumPractice/ && mvn test");
    }
}
