package AndroidApiDemosDebug.TestsCases;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.appiumDriver;


public class General_Store_Add_To_Cart_Verify
{
    static General_Store_Create_User_And_Login user = new General_Store_Create_User_And_Login();

    @Test(priority = 1)
    public static void login() throws InterruptedException
    {
        user.login();

        // #1 scroll find the "air jordan 6 rings"
        appiumDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector())" +
                ".scrollIntoView(text(\"Jordan 6 Rings\"))");

        // first item
        appiumDriver.findElements(By.id("com.androidsample.generalstore:id/productAddCart")).get(0).click();
        double price1 = Double.parseDouble(appiumDriver.findElementsById("com.androidsample.generalstore:id/productPrice").get(0).getText().replace("$",""));
        System.out.println("price 1 is "+price1);
        String addedToCart1 = appiumDriver.findElements(By.id("com.androidsample.generalstore:id/productAddCart")).get(0).getText();
        Assert.assertEquals(addedToCart1,"ADDED TO CART");
        Assert.assertEquals(price1,165.0);
        System.out.println("Thee first shoe is "+addedToCart1+ " and the price is "+price1 );
        Thread.sleep(3000);
        // second item
        // the reason for the second item having the get(0) in dex here is that once the first item is clicked, the text changes to the "ADDED TO CART"
        // there fore there is only one text left as "ADD TO CART" which is get(0)
        // update, eventhough it was explained in the udemy video as this, in my observation, I had to use get(0) for the first item
        // and get(1) on the second item
        appiumDriver.findElements(By.id("com.androidsample.generalstore:id/productAddCart")).get(1).click();
        double price2 = Double.parseDouble(appiumDriver.findElementsById("com.androidsample.generalstore:id/productPrice").get(1).getText().replace("$",""));
        String addedToCart2 = appiumDriver.findElements(By.id("com.androidsample.generalstore:id/productAddCart")).get(1).getText();
        Assert.assertEquals(addedToCart1,"ADDED TO CART");
        Assert.assertEquals(price2,115.0);
        System.out.println("Thee second shoe is "+addedToCart2+ " and the price is "+price2 );
        Thread.sleep(3000);
        //click on the cart icon
        appiumDriver.findElementById("com.androidsample.generalstore:id/appbar_btn_cart").click();
        // verify total
        String totalPrice = appiumDriver.findElementById("com.androidsample.generalstore:id/totalAmountLbl").getText();
        Assert.assertEquals(price1+price2, 280.0);
        System.out.println(" int price 1 "+price1+price2);

        System.out.println("Thee Total price is "+totalPrice);



    }

    public static void main(String[] args)
    {

        String a ="$165.0";
        String b ="$115.0";

        double aa = Double.parseDouble((a.replace("$","")));
        double bb = Double.parseDouble((b.replace("$","")));
        double total = aa+bb;

        System.out.println("aa is "+aa+" bb is "+ bb+" and total is "+total);

    }

}
