package AndroidApiDemosDebug.TestsCases;

import org.testng.annotations.Test;

public class MVN_Smoke
{
    public static void main(String[] args)
    {
        mvnCmdTest();
    }
    @Test
    public static void mvnCmdTest()
    {
        System.out.println
                ("This is the change from develop branch This is to run SMOKE TC cd /Users/kevin/Documents/Automation/Gitlab/ && mvn test -PSmoke" +
                "Its in the gitlab repo.This is to run SMOKE TC cd /Users/kevin/Documents/Automation/Gitlab/ && mvn test -PSmoke" +
                "This is the change from develop branch This is to run SMOKE TC cd /Users/kevin/Documents/Automation/Gitlab/ && mvn test -PSmoke" +
                "<profiles>\n" +
                "    <profile>\n" +
                "        <id>Regression</id>\n" +
                "            <build>\n" +
                "                <plugin>\n" +
                "                    <groupId>org.apache.maven.plugins</groupId>\n" +
                "                    <artifactId>maven-surefire-plugin</artifactId>\n" +
                "                    <version>3.0.0-M4</version>\n" +
                "                    <configuration>\n" +
                "                        <suiteXmlFiles>\n" +
                "                            <suiteXmlFile>src/test/java/AndroidApiDemosDebug/TestNGXML/TestNG_Regression.xml\n" +
                "                            </suiteXmlFile>\n" +
                "                        </suiteXmlFiles>\n" +
                "                    </configuration>\n" +
                "                </plugin>\n" +
                "            </build>\n" +
                "    </profile>\n" +
                "    <profile>\n" +
                "        <id>Smoke</id>\n" +
                "            <build>\n" +
                "                <plugin>\n" +
                "                    <groupId>org.apache.maven.plugins</groupId>\n" +
                "                    <artifactId>maven-surefire-plugin</artifactId>\n" +
                "                    <version>3.0.0-M4</version>\n" +
                "                    <configuration>\n" +
                "                        <suiteXmlFiles>\n" +
                "                            <suiteXmlFile>src/test/java/AndroidApiDemosDebug/TestNGXML/TestNG_Smoke.xml\n" +
                "                            </suiteXmlFile>\n" +
                "                        </suiteXmlFiles>\n" +
                "                    </configuration>\n" +
                "                </plugin>\n" +
                "            </build>\n" +
                "    </profile>\n" +
                "</profiles>");
    }
}
