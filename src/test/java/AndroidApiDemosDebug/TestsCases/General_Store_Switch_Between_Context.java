package AndroidApiDemosDebug.TestsCases;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import java.util.Set;

import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.appiumDriver;


public class General_Store_Switch_Between_Context
{
    static General_Store_Create_User_And_Login user = new General_Store_Create_User_And_Login();

    @Test(priority = 1)
    public static void context()
    {
        //TO-DO: need to understand more about context switching

        user.login();
        // #1 scroll find the "air jordan 6 rings"
        appiumDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector())" +
                ".scrollIntoView(text(\"Jordan 6 Rings\"))");
        appiumDriver.findElements(By.id("com.androidsample.generalstore:id/productAddCart")).get(0).click();
        appiumDriver.findElementById("com.androidsample.generalstore:id/appbar_btn_cart").click();
        System.out.println("The context on the 'CART' is "+appiumDriver.getContextHandles());
        appiumDriver.findElement(By.id("com.androidsample.generalstore:id/btnProceed")).click();

        try
        {
            Thread.sleep(30000);
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        Set<String> contextNames = appiumDriver.getContextHandles();

        for (String contextName : contextNames)
        {
            System.out.println("The context [] "+contextNames+ " is "+contextName); //prints out something like NATIVE_APP \n WEBVIEW_1
        }

        //System.out.println("The context Switching the to'WEBVIEW_1'");
       // appiumDriver.context("WEBVIEW_com.androidsample.generalstore");
        System.out.println("The context on the 'Google' is "+appiumDriver.getContextHandles());

        System.out.println("The context Switching the to first array [1]");
        appiumDriver.context(String.valueOf(contextNames.toArray()[1]));
        System.out.println("The context after switching to the first array [1] is "+appiumDriver.getContextHandles());

    }

    public static void main(String[] args)
    {


    }

}
