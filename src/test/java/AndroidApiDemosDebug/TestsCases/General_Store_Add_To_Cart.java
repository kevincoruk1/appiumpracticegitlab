package AndroidApiDemosDebug.TestsCases;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.appiumDriver;


public class General_Store_Add_To_Cart
{
    static General_Store_Create_User_And_Login user = new General_Store_Create_User_And_Login();

    @Test(priority = 1)
    public static void login() throws InterruptedException
    {
        user.login();

        // #1 scroll find the "air jordan 6 rings"
        appiumDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector())" +
                ".scrollIntoView(text(\"Jordan 6 Rings\"))").click();
        // find the count of element on the current screen
        int count = appiumDriver.findElementsById("com.androidsample.generalstore:id/productName").size();


        // loop through element and click on add to cart
        for (int i = 0; i < count; i++)
        {
            String productName = appiumDriver.findElements(By.id("com.androidsample.generalstore:id/productName")).get(i).getText();
            if (productName.equalsIgnoreCase("Jordan 6 Rings"))
            {
                // add jordan 6 rings to cart
                appiumDriver.findElementsById("com.androidsample.generalstore:id/productAddCart").get(i).click();
                //click on the cart icon
                appiumDriver.findElementById("com.androidsample.generalstore:id/appbar_btn_cart").click();
                //verify item
                String itemName = appiumDriver.findElementById("com.androidsample.generalstore:id/productName").getText();
                String itemPrice = appiumDriver.findElementById("com.androidsample.generalstore:id/productPrice").getText();
                String totalPrice = appiumDriver.findElementById("com.androidsample.generalstore:id/totalAmountLbl").getText();
                System.out.println("Yes its the '" + itemName+"' and its price is: "+itemPrice+ " and the total price is: "+totalPrice);
                Assert.assertEquals(itemName,"Jordan 6 Rings");
                Assert.assertEquals(itemPrice, "$165.0");
                Assert.assertEquals(totalPrice,"$ 165.0");
                Thread.sleep(5000);
                break;
            }


        }


    }


}
