package AndroidApiDemosDebug.TestsCases;

import org.testng.Assert;
import org.testng.annotations.Test;

import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.appiumDriver;

public class General_Store_Login_Error
{
    @Test(priority = 1)
    public static void login() throws InterruptedException
    {
        System.out.println("login method executed ?");
        // click on select country drop down
        appiumDriver.findElementById("com.androidsample.generalstore:id/spinnerCountry").click();
        // select the country
        // if below line doesnt work with some iOS version use the other one
        appiumDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"Belarus\"))" ).click();
        //appiumDriver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textMatches(\"Belarus\").instance(0))")).click();
        // enter your name
        //appiumDriver.findElementById("com.androidsample.generalstore:id/nameField").sendKeys("myName is Kev");
        //Select gender
        appiumDriver.findElementById("com.androidsample.generalstore:id/radioFemale").click();
        //click on lets shop button
        appiumDriver.findElementById("com.androidsample.generalstore:id/btnLetsShop").click();
        // get the error message
        String LoginError = appiumDriver.findElementByXPath("//android.widget.Toast[1]").getAttribute("name"); //Please enter your name
        Assert.assertEquals("Please enter your name xx",LoginError);





    }


}
