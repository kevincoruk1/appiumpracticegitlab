package AndroidApiDemosDebug.CommonLibrary;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class Extent_Report
{
    public static String timeStamp = new SimpleDateFormat("MMMM dd yyyy HH:mm:ss").format(Calendar.getInstance().getTime());//.replace(":", "+").replace(" ", "=");
    private static String reportFileName = "Regression Test On " + timeStamp + ".html";
    private static String reportDirectory = System.getProperty("user.dir") + "/src/test/java/AndroidApiDemosDebug/Result/";
    private static String reportPath = reportDirectory + reportFileName ;


    public static ExtentTest testNGTest = null;
    static Map<Integer, ExtentTest> extentTestMap = new HashMap<Integer, ExtentTest>();
    static ExtentReports extentReports = getInstance();

    public static synchronized ExtentTest getTestNGTest()
    {
        return (ExtentTest) extentTestMap.get((int) (long) (Thread.currentThread().getId()));
    }

    public static synchronized void endTest()
    {
        extentReports.flush();
    }

    public static synchronized ExtentTest startTest(String testName)
    {
        testNGTest = extentReports.createTest(testName);
        extentTestMap.put((int) (long) (Thread.currentThread().getId()), testNGTest);
        return testNGTest;
    }

    public static ExtentReports getInstance()
    {
        if (extentReports == null)
            createInstance();
        return extentReports;
    }

    public static ExtentReports createInstance() 
    {
        ExtentHtmlReporter extentHtmlReporter = new ExtentHtmlReporter(reportPath);
        extentHtmlReporter.config().setTheme(Theme.STANDARD);
        extentHtmlReporter.config().setDocumentTitle("Regression Test on "+ timeStamp);
        extentHtmlReporter.config().setEncoding("utf-8");
        extentHtmlReporter.config().setReportName("Regression Test on "+ timeStamp);

        extentReports = new ExtentReports();
        extentReports.attachReporter(extentHtmlReporter);
        return extentReports;
    }

//    public static void logFailMessage (String failMsg )
//    {
//        extentTest.fail(failMsg);
//    }
//    public static void reportLog(String expectedElement, String passMsg, String failMsg)
//    {
//        try
//        {
//            findElement(expectedElement).isDisplayed();
//            extentTest.pass(passMsg);
//        } catch (Exception e)
//        {
//            extentTest.fail(failMsg);
//            assert false;
//        }
//    }

}
