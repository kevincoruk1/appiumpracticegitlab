package AndroidApiDemosDebug.CommonLibrary;

import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import org.openqa.selenium.WebElement;

import java.time.Duration;

import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.getAppiumDriver;
import static io.appium.java_client.touch.offset.ElementOption.element;

public class Swipe
{
    public static void swipeToElement(WebElement from, WebElement to)
    {


        TouchAction swipe = new TouchAction(getAppiumDriver());

        //swipe from one element to another by first getting the element's coordinates
        //swipe.press(PointOption.point(fromElement)).waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2))).moveTo(PointOption.point(toElement)).release().perform();

        // Swipe from one element to another
        //swipe.longPress(element(from)).moveTo(element(to)).release().perform();
        swipe.longPress(element(from)).waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2))).moveTo(element(to)).release().perform();



    }

}
