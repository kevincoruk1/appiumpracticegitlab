package AndroidApiDemosDebug.CommonLibrary;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Android_Driver
{

    public static void main(String[] args)
    {
        System.out.println("This is to test configuration file: " + getPropertyByKey("platformName"));
    }


    public static DesiredCapabilities deCaps = null;
    public static AndroidDriver<AndroidElement> appiumDriver;

    public static void startAppiumDriver()
    {
        deCaps = new DesiredCapabilities();
        //ApiDemos-debug.apk
        deCaps.setCapability(MobileCapabilityType.DEVICE_NAME, getPropertyByKey("deviceName"));
        deCaps.setCapability(MobileCapabilityType.PLATFORM_NAME, getPropertyByKey("platformName"));
        deCaps.setCapability("avd", getPropertyByKey("deviceName"));
        deCaps.setCapability(MobileCapabilityType.NO_RESET, "true");
        deCaps.setCapability(MobileCapabilityType.AUTOMATION_NAME, getPropertyByKey("automationName"));
        deCaps.setCapability("chromedriverExecutableDir",System.getProperty("user.dir") +"/src/test/java/AndroidApiDemosDebug/Resources");
        //deCaps.setCapability("chromedriver_autodownload",true);

       // deCaps.setCapability(MobileCapabilityType.APP, System.getProperty("user.dir") + getPropertyByKey("application"));

        //General-Store.apk
        deCaps.setCapability(MobileCapabilityType.APP, System.getProperty("user.dir") + "/src/test/java/AndroidApiDemosDebug/Resources/General-Store.apk");
        deCaps.setCapability("appPackage", "com.androidsample.generalstore");
        deCaps.setCapability("appActivity","com.androidsample.generalstore.SplashActivity");



        // kill all note before starting the appium server
        try
        {
            Runtime.getRuntime().exec("killall node");
            System.out.println("killall note run");
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        URL url = getAppiumDriverLocalService().getUrl();   //for starting the appium server automatically
        System.out.println("the url is " + url);            //for starting the appium server automatically
        appiumDriver = new AndroidDriver<>(url, deCaps);   //for starting the appium server automatically
        appiumDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }


    public static void stopAppiumDriver()
    {
        appiumDriver.quit();
        appiumDriver = null;
        service = getAppiumDriverLocalService();
        service.stop();
        try
        {
            Runtime.getRuntime().exec("killall node");
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        System.out.println("Appium Server stopped");
    }

    public static AndroidDriver getAppiumDriver()
    {
        if (appiumDriver == null)
        {
            startAppiumDriver();
        }
        return appiumDriver;
    }

    // start appium service by default local url
    public static AppiumDriverLocalService service;

    public static AppiumDriverLocalService getAppiumDriverLocalService()
    {
        if (service == null)
        {
            service = AppiumDriverLocalService.buildDefaultService();
            service.start();
        }
        return service;
    }

    //        // start the appium server with the provided url
//        public static AppiumDriverLocalService getAppiumDriverLocalService_1 (String ipAddress, String port)
//        {
//            if (service == null)
//            {
//                AppiumServiceBuilder appiumServiceBuilder = new AppiumServiceBuilder();
//                appiumServiceBuilder.usingPort(Integer.parseInt(port));
//                appiumServiceBuilder.withIPAddress(ipAddress);
//                service = AppiumDriverLocalService.buildService(appiumServiceBuilder);
//                service.start();
//            }
//            return service;
//        }
//*************************************************************************
    // read the configuration file
    static Properties configurationFile = new Properties();

    public static String getPropertyByKey(String key)
    {
        try
        {
            // load configuration file first
            configurationFile.load(new FileInputStream(new File(System.getProperty("user.dir") + "/src/test/java/AndroidApiDemosDebug/Configuration/Configuration.Properties")));
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        return configurationFile.getProperty(key);
    }
//**************************************************************************


}
