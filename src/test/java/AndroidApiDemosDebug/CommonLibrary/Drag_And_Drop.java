package AndroidApiDemosDebug.CommonLibrary;

import io.appium.java_client.TouchAction;
import org.openqa.selenium.WebElement;

import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.getAppiumDriver;
import static io.appium.java_client.touch.offset.ElementOption.element;

public class Drag_And_Drop
{
    public static void dragAndDrop(WebElement from, WebElement to)
    {
        TouchAction action = new TouchAction(getAppiumDriver());
        action.longPress(element(from)).moveTo(element(to)).release().perform();
    }
}
