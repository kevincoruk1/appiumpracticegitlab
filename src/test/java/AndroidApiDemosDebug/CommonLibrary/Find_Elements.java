package AndroidApiDemosDebug.CommonLibrary;

import io.appium.java_client.MobileElement;

import java.util.List;

import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.getAppiumDriver;


public class Find_Elements
{
    // find element and return element
    public static MobileElement findElement(String runTimeElement)
    {
        try
        {
            MobileElement element;
            if (runTimeElement.startsWith("id="))
            {
                element = (MobileElement) getAppiumDriver().findElementById(runTimeElement.replace("id=", ""));
                return element;
            }
            else if (runTimeElement.startsWith("accessibilityId="))
            {
                element = (MobileElement) getAppiumDriver().findElementByAccessibilityId(runTimeElement.replace("accessibilityId=", ""));
                return element;
            }
            else if (runTimeElement.startsWith("xpath="))
            {
                element = (MobileElement) getAppiumDriver().findElementByXPath(runTimeElement.replace("xpath=", ""));
                return element;
            }
            else if (runTimeElement.startsWith("name="))
            {
                element = (MobileElement) getAppiumDriver().findElementByName(runTimeElement.replace("name=", ""));
                return element;
            }
            else if (runTimeElement.startsWith("predicate="))
            {
//                element = (MobileElement) getAppiumDriver().findElementByIosNsPredicate(runTimeElement.replace("predicate=", ""));
//                return element;
            }
            else
            {
                return (MobileElement) getAppiumDriver().findElementByAccessibilityId(runTimeElement);
               // return element;
            }
        } catch (Exception e)
        {
            // log to extend report
//            logFailMessage("'findElementDynamically' Method Has Failed. It Cannot Find The '" + runTimeElement + "' Element On The Screen");
//            logFailMessage(e.getMessage());
            // print on console
            System.out.println("Find_Elements >>> findElement " + e);
        }
        return null;
    }

    // find multiple elements and return elements
    public static List<MobileElement> findMultipleElements(String runTimeElement)
    {
        try
        {
            List<MobileElement> elements;
            if (runTimeElement.startsWith("id="))
            {
                elements = getAppiumDriver().findElementsById(runTimeElement.replace("id=", ""));
                return elements;
            }
            else if (runTimeElement.startsWith("accessibilityId="))
            {
                elements = getAppiumDriver().findElementsByAccessibilityId(runTimeElement.replace("accessibilityId=", ""));
                return elements;
            }
            else if (runTimeElement.startsWith("xpath="))
            {
                elements = (List<MobileElement>) getAppiumDriver().findElementsByXPath(runTimeElement.replace("xpath=", ""));
                return elements;
            }
            else if (runTimeElement.startsWith("name="))
            {
                elements = (List<MobileElement>) getAppiumDriver().findElementsByName(runTimeElement.replace("name=", ""));
                return elements;
            }
            else if (runTimeElement.startsWith("predicate="))
            {
//                elements = (List<MobileElement>) getAppiumDriver().findElementsByIosNsPredicate(runTimeElement.replace("predicate=", ""));
//                return elements;
            }
            else
            {
                elements = (List<MobileElement>) getAppiumDriver().findElementsByAccessibilityId(runTimeElement);
                return elements;
            }
        } catch (Exception e)
        {
            // log to extend report
//            logFailMessage("Method Has Failed. It Cannot Find The '" + runTimeElement + "' Element On The Screen");
//            logFailMessage(e.getMessage());
            // print on console
            System.out.println("Find_Elements >>> findMultipleElements " + e);
        }
        return null;
    }



}

