package AndroidApiDemosDebug.CommonLibrary;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;

import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.appiumDriver;

public class Scroll
{


    public static MobileElement  scrollToElement1(String element1)
    {


        MobileElement element = (MobileElement)appiumDriver.findElement(MobileBy.AndroidUIAutomator(
        "new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().text(\"" + element1 + "\"))"));
        return element;

    }

    public static MobileElement  scrollToElement2(String resourceId, String text, int index)
    {


    MobileElement element = (MobileElement)appiumDriver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector()." +
            "resourceId(\"" + resourceId + "\")).scrollIntoView(new UiSelector()" +
            ".textMatches(\""+text+"\").instance("+index+"))"));
        return element;

    }

     /*
        // #1 scroll to element when the text or any attribute is unique
        appiumDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"WebView\"))" ).click();

        //#2 scroll
        appiumDriver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector()" +
                ".scrollable(true).instance(0)).scrollIntoView(new UiSelector().textMatches(\"Jordan 6 Rings\").instance(0))")).click();

        //#3 scroll when there are multiple not unique elements on the screen.
        appiumDriver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector()" +
                ".resourceId(\"com.androidsample.generalstore:id/rvProductList\")).scrollIntoView(new UiSelector()" +
                ".textMatches(\"Jordan 6 Rings\").instance(0))"));

 */

}
