package AndroidApiDemosDebug.CommonLibrary;

import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import org.openqa.selenium.WebElement;

import java.time.Duration;

import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.getAppiumDriver;

public class Long_Press
{
    public static void longPress(WebElement peopleNames)
    {
        TouchAction tap = new TouchAction(getAppiumDriver());
        //tap.tap(Pointoption tapOptions)>> tap'in element by its x,y coordinate
        //tap.tap(TapOption tapOptions) >> tap'in element by element attributes

        // long press on the elment
        tap.longPress(LongPressOptions.longPressOptions().withElement(ElementOption.element(peopleNames)))
                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2))).release().perform();
    }
}
