package AndroidApiDemosDebug.CommonLibrary;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;

import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.appiumDriver;

public class Take_Screenshot
{
    // need to call this method on 'onTestFailure' method on 'TestNG_Listener' class
    // so that it will take screenshot when test fails
    public static void screenshotOnFailure(String fileName) throws IOException
    {
       File screenShot = ((TakesScreenshot)appiumDriver).getScreenshotAs(OutputType.FILE);
       FileUtils.copyFile(screenShot, new File(System.getProperty("user.dir")+"/src/test/java/AndroidApiDemosDebug/Result/Screenshot/"+fileName+".png"));

    }
}
