package AndroidApiDemosDebug.CommonLibrary;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.IOException;

import static AndroidApiDemosDebug.CommonLibrary.Take_Screenshot.screenshotOnFailure;

public class TestNG_Listeners implements ITestListener
{
    private static String getTestMethodName(ITestResult iTestResult)
    {
        return iTestResult.getMethod().getConstructorOrMethod().getName();
    }

    //Extent Report Declarations
    public static ExtentTest extentTest = null;
    private static ExtentReports extent = Extent_Report.createInstance();
    public static ThreadLocal<ExtentTest> test = new ThreadLocal<>();



    @Override
    public synchronized void onTestStart(ITestResult result) 
    {
        System.out.println((result.getMethod().getMethodName() + " started!"));
        extentTest = extent.createTest(result.getMethod().getMethodName(), result.getMethod().getDescription());
        test.set(extentTest);
    }

    @Override
    public synchronized void onTestSuccess(ITestResult result) 
    {
        System.out.println((result.getMethod().getMethodName() + " passed!"));
    }

    @Override
    public synchronized void onTestFailure(ITestResult result) 
    {
        System.out.println((result.getMethod().getMethodName() + " failed!"));
        test.get().fail(result.getThrowable());

        // take screenshot
        String name = result.getMethod().getMethodName();
        try
        {
            screenshotOnFailure(name);

        } catch (IOException e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public synchronized void onTestSkipped(ITestResult result) 
    {
        System.out.println((result.getMethod().getMethodName() + " skipped!"));
        test.get().skip(result.getThrowable());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) 
    {
        System.out.println((result.getMethod().getMethodName()+" TestFailedButWithinSuccessPercentage"));
    }

    @Override
    public synchronized void onFinish(ITestContext context)
    {
        extent.flush();
    }
}
