package AndroidApiDemosDebug.ObjectRepository;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.appiumDriver;

public class Views
{

    public Views(AppiumDriver appiumDriver)
    {
        PageFactory.initElements(new AppiumFieldDecorator(appiumDriver), this);

    }
    //method #1
    @AndroidFindBy(accessibility= "Drag and Drop")
    public MobileElement Drag_And_Drop;

    //method #2
    public MobileElement getDragAndDrop()
    {
        return appiumDriver.findElementByAccessibilityId("Drag and Drop");
    }

}

