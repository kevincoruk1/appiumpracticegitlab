package AndroidApiDemosDebug.ObjectRepository;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.appiumDriver;

public class Drag_And_Drop
{

    public Drag_And_Drop(AppiumDriver appiumDriver)
    {
        PageFactory.initElements(new AppiumFieldDecorator(appiumDriver), this);

    }
    @AndroidFindBy(className= "android.view.View")
    private List<AndroidElement> Element;

//    public List<AndroidElement> getElements()
//    {
//        return Element;
//    }

    public List<AndroidElement> getElements()
    {
        return appiumDriver.findElementsByClassName("android.view.View");
    }

}

