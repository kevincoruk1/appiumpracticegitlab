package AndroidApiDemosDebug.ObjectRepository;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

import static AndroidApiDemosDebug.CommonLibrary.Android_Driver.appiumDriver;
import static AndroidApiDemosDebug.CommonLibrary.Find_Elements.findElement;

public class HomePage
{

    public HomePage(AppiumDriver appiumDriver)
    {
        PageFactory.initElements(new AppiumFieldDecorator(appiumDriver), this);

    }
    //method #1
    @AndroidFindBy(accessibility= "Views")
    public MobileElement views1;


    //method #2
    public MobileElement views2()
    {
        return appiumDriver.findElementByAccessibilityId("Views");
    }

    //method #3 use find use 'findElement' method
    // its not working
    public MobileElement views3()
    {
        return (MobileElement)findElement("Views");

    }



}

