package AppiumUICatalog.TestCases;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import static AppiumUICatalog.CommonLibrary.iOS_Driver.startAppiumDriver;
import static AppiumUICatalog.CommonLibrary.iOS_Driver.stopAppiumDriver;

public class Before_And_After_Suite
{

    @BeforeSuite
    public static void startAppiumServerAndDriverBeforeSuite()
    {
        startAppiumDriver();
        System.out.println("Appium Server started");
    }


    @AfterSuite
    public static void stopAppiumServerAndDriverAfterSuite()
    {
        stopAppiumDriver();
        System.out.println("Appium Server stopped");
    }

//    @BeforeMethod
//    public void startApplication()
//    {
//        System.out.println("start app method called");
//        appiumDriver.launchApp();
//    }
//    @AfterTest
//    public void stopApplication()
//    {
//        System.out.println("stop app method called");
//        appiumDriver.closeApp();
//    }




}
