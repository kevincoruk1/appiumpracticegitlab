package AppiumUICatalog.CommonLibrary;

import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static AppiumUICatalog.CommonLibrary.Configuration.getPropertiesByKey;
import static AppiumUICatalog.CommonLibrary.Configuration.loadConfigurationFile;

public class iOS_Driver
{

    public static void main(String[] args)
    {
        loadConfigurationFile();
        System.out.println("This is to test configuration file: " +getPropertiesByKey("platformName"));
    }

        public static AppiumDriverLocalService service;
        public static DesiredCapabilities deCaps = null;
        public static IOSDriver<MobileElement> myiOSDriver;

        public static void startAppiumDriver ()
        {
            loadConfigurationFile();
            deCaps = new DesiredCapabilities();

            deCaps.setCapability(MobileCapabilityType.DEVICE_NAME, getPropertiesByKey("deviceName"));
            deCaps.setCapability(MobileCapabilityType.PLATFORM_NAME,getPropertiesByKey("platformName"));
            deCaps.setCapability(MobileCapabilityType.PLATFORM_VERSION,getPropertiesByKey("platformVersion"));
            deCaps.setCapability(MobileCapabilityType.AUTOMATION_NAME, getPropertiesByKey("automationName"));
            deCaps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 10);
            deCaps.setCapability("usePrebuiltWDA", "true");
            deCaps.setCapability(MobileCapabilityType.NO_RESET, true);
            deCaps.setCapability(MobileCapabilityType.FULL_RESET, false);
            deCaps.setCapability(MobileCapabilityType.APP, System.getProperty("user.dir") +getPropertiesByKey("application"));
            //deCaps.setCapability(MobileCapabilityType.BROWSER_NAME, "Safari");
//            deCaps.setCapability(MobileCapabilityType.UDID, getPropertiesByKey("deviceUDID"));  //real device
//            deCaps.setCapability("xcodeOrgId", "Your Team ID from app.developer");                //real device
//            deCaps.setCapability("xcodeSigningId", "iPhone Developer");      //real device
//            deCaps.setCapability("updateWDABundleId", "com.vfc.WDR.WebDriverAgentRunner");    //real device



//            try
//            {
//                appiumDriver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"),deCaps ); //for starting the appium server manually
//            } catch (MalformedURLException e)
//            {
//                e.printStackTrace();
//            }

            // kill all note before starting the appium server
            try
            {
                Runtime.getRuntime().exec("killall node");
                System.out.println("killall note run");
            } catch (IOException e)
            {
                e.printStackTrace();
            }
            URL url = getAppiumDriverLocalService().getUrl();   //for starting the appium server automatically
            System.out.println("the url is " + url);            //for starting the appium server automatically
            myiOSDriver = new IOSDriver<MobileElement>(url, deCaps);   //for starting the appium server automatically
            myiOSDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        }


        public static void stopAppiumDriver ()
        {
            myiOSDriver.quit();
            myiOSDriver = null;
            service = getAppiumDriverLocalService();
            service.stop();
            try
            {
                Runtime.getRuntime().exec("killall node");
            } catch (IOException e)
            {
                e.printStackTrace();
            }
            System.out.println("Appium Server stopped");
        }

        public static IOSDriver getMyiOSDriver()
        {
            if (myiOSDriver == null)
            {
                startAppiumDriver();
            }
            return myiOSDriver;
        }

        // start appium service by default local url
        public static AppiumDriverLocalService getAppiumDriverLocalService ()
        {
            if (service == null)
            {
                service = AppiumDriverLocalService.buildDefaultService();
                service.start();
            }
            return service;
        }

//        // start the appium server with the provided url
//        public static AppiumDriverLocalService getAppiumDriverLocalService_1 (String ipAddress, String port)
//        {
//            if (service == null)
//            {
//                AppiumServiceBuilder appiumServiceBuilder = new AppiumServiceBuilder();
//                appiumServiceBuilder.usingPort(Integer.parseInt(port));
//                appiumServiceBuilder.withIPAddress(ipAddress);
//                service = AppiumDriverLocalService.buildService(appiumServiceBuilder);
//                service.start();
//            }
//            return service;
//        }



}
