package AppiumUICatalog.CommonLibrary;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static io.appium.java_client.touch.offset.PointOption.point;

public class AppiumSwipe extends Base
{
    public static void swipeVertically(AppiumDriver<MobileElement> driver, double startPercentage, double endPercentage) throws MalformedURLException
    {
        Dimension size = driver.manage().window().getSize();
        int width = (int) (size.width / 2);
        int startY = (int) (size.getHeight() * startPercentage);
        int endY = (int) (size.getHeight() * endPercentage);
        new TouchAction(driver).longPress(point(width, startY)).moveTo(point(width, endY)).release().perform();

    }

    // this method will scroll up/down
    public static void scroll (String direction)
    {
        Dimension size = driver.manage().window().getSize();
        int xCordinate = size.getHeight() / 2;
        int startYCordinate = 0;
        int endYCordinate = 0;

        switch (direction)
        {
            case "UP":
                System.out.println("scroll: going uupppp");
                startYCordinate = (int) (size.getHeight() * 0.8);
                endYCordinate = (int) (size.getHeight() * 0.2);
                break;

            case "DOWN":
                System.out.println("scroll: going downnnn");
                startYCordinate = (int) (size.getHeight() * 0.2);
                endYCordinate = (int) (size.getHeight() * 0.8);
                break;

        }
        TouchAction touch = new TouchAction(driver);
        touch.press(PointOption.point(xCordinate,startYCordinate))
             .waitAction(WaitOptions.waitOptions(Duration.ofMillis(1000)))
             .moveTo(PointOption.point(xCordinate, endYCordinate)).release().perform();

    }

    // this method will return true/false for element displayed or not
    public static boolean isDisplayed(final By e)
    {
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        try
        {
            WebDriverWait wait = new WebDriverWait(driver, 2);
            return wait.until(new ExpectedCondition<Boolean>()
                  {
                      public Boolean apply(WebDriver driver)
                      {
                          if (driver.findElement(e).isDisplayed())
                          {
                              System.out.println("isDisplayed: oh yeah i have found the element "+e);
                              return true;
                          } else
                          {
                              System.out.println("isDisplayed: nope i didnt find the element "+e);
                              return false;
                          }
                      }
                  });
        } catch (Exception ex)
        {
            return false;
        }
    }

    // this method will loop through scroll and isDisplayed method until the element is displayed on the screen
    public static void scrollToElement(By e, String direction)
    {
        for (int i = 0; i>=3; i++ )
        {
           if (isDisplayed(e))
           {
               System.out.println("scrollToElement: i found the element "+e);
               break;
           } else
           {
               System.out.println("scrollToElement: no i did not find the element "+e);
               if (direction.equalsIgnoreCase("UP"))
               {
                   scroll("UP");
               }else
               {
                   scroll("DOWN");
               }
           }
        }
    }
}
