package AppiumUICatalog.CommonLibrary;

import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Configuration
{

    public static void main(String[] args)
    {
        loadConfigurationFile();
        System.out.println("This is to test config file "+getPropertiesByKey("deviceName"));
    }

    public static DesiredCapabilities deCaps=null;
    static Properties configurationFile = new Properties();

    public static void loadConfigurationFile()
    {
        deCaps = new DesiredCapabilities();
        try
        {
            configurationFile.load(new FileInputStream(new File(System.getProperty("user.dir")+"/src/test/java/AppiumUICatalog/Configuration/Configuration.properties")));

        } catch (IOException e)
        {
            e.printStackTrace();
        }


    }

    public static String getPropertiesByKey(String key)
    {
        return configurationFile.getProperty(key);
    }




}
