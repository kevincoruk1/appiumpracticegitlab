package AppiumUICatalog.CommonLibrary;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Base
{
    public static AppiumDriver<MobileElement> driver;

    static
    {
        try
        {
            driver = capabilities();
        } catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
    }

    public static AppiumDriver<MobileElement> capabilities() throws MalformedURLException
    {
        DesiredCapabilities d = new DesiredCapabilities();
        d.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone SE");
        d.setCapability(MobileCapabilityType.PLATFORM_VERSION, "12.4");
        d.setCapability(MobileCapabilityType.PLATFORM_NAME, "IOS");
        d.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
        //d.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.IOS_XCUI_TEST);
        d.setCapability(MobileCapabilityType.NO_RESET, true);
        d.setCapability(MobileCapabilityType.FULL_RESET, false);
        d.setCapability(MobileCapabilityType.APP,System.getProperty("user.dir")+ "/src/test/java/AppiumUICatalog/Resources/Application/UICatalog.app");
        d.setCapability("usePrebuiltWDA", "true");
        driver = new IOSDriver<>(new URL("http://127.0.0.1:4723/wd/hub"),d);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return driver;
    }

}
