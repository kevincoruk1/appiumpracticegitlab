package AppiumUICatalog.CommonLibrary;


import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.Dimension;

import static AppiumUICatalog.CommonLibrary.iOS_Driver.myiOSDriver;

public class Scroll
{


    public static void scrollToElement(String element, String direction, int count)
    {
        Dimension size = myiOSDriver.manage().window().getSize();
        int X = 0;
        int startY = 0;
        int endY = 0;

        for (int i = 0; i <= count; i++)
        {
            if (myiOSDriver.findElementByAccessibilityId(element).isDisplayed() == true)
            {
                break;
            }
            else
            {
                if (direction == "UP")
                {
                    startY = (int) (size.height * 0.70);
                    endY = (int) (size.height * 0.30);
                    X = (size.width / 2);
                    new TouchAction(myiOSDriver).longPress(PointOption.point(X, startY)).moveTo(PointOption.point(X, endY)).release().perform();
                } else
                {
                    startY = (int) (size.height * 0.30);
                    endY = (int) (size.height * 0.70);
                    X = (size.width / 2);
                    new TouchAction(myiOSDriver).longPress(PointOption.point(X, startY)).moveTo(PointOption.point(X, endY)).release().perform();
                }
            }
        }
    }
}
/*                      |
                        0
                        |
x = w = 320    <-0------|-----320->
y = h = 568             |
                       568
deviceName= 4, 118
size(label) = 261, 140

    style(label) = 48, 116
location (label) = 48, 183 (so the height of the cell is 67)
5% of the width is 16.0
10% of the width is 32.0
15% of the width is 48.0
20% of the width is 64.0
25% of the width is 80.0
30% of the width is 96.0
35% of the width is 112.0
40% of the width is 128.0
45% of the width is 144.0
50% of the width is 160.0
5% of the height is 28.400000000000002
10% of the height is 56.800000000000004
15% of the height is 85.2
20% of the height is 113.60000000000001
25% of the height is 142.0
30% of the height is 170.4
35% of the height is 198.79999999999998
40% of the height is 227.20000000000002
45% of the height is 255.6
50% of the height is 284.0


 */
