package TestNG.TestNGListeners;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

// you can use testng listeners to define what to run before/after tes cases or method/class
// for testng listeners to work, we have to define in xml file as well.
//  This testng listener is defined in "TestNG_Listener.xml" file

public class testngListeners implements ITestListener
{

    @Override
    public void onStart(ITestContext context)
    {
        System.out.println("onStart will run at the START of the 'Java class' ");
    }

    @Override
    public void onTestStart(ITestResult result)
    {
        System.out.println("onteststart");
    }

    @Override
    public void onTestSuccess(ITestResult result)
    {
        System.out.println("onTestSuccess");
    }

    @Override
    public void onTestFailure(ITestResult result)
    {
        System.out.println("onTestFailure: The failing method is 1 "+ result.getMethod().getMethodName() );
        System.out.println("onTestFailure: The failing method is 2 "+ result.getName() );
    }

    @Override
    public void onTestSkipped(ITestResult result)
    {
        System.out.println("onTestSkipped");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result)
    {
        System.out.println("onTestFailedButWithinSuccessPercentage");
    }
    @Override
    public void onFinish(ITestContext context)
    {
        System.out.println("onFinish will run at the END of the 'Java class' ");
    }
}
