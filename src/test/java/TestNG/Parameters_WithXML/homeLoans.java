package TestNG.Parameters_WithXML;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class homeLoans

{
    @Parameters({"URL"})
    @Test
    public void testHomeLoans(String urlName)
    {
        System.out.println("This is testing the Parameter.HOME loans function.");
        System.out.println("Method: testHomeLoans, The url that comes from testNG_Parameters.xml file is " + urlName);
    }


}

