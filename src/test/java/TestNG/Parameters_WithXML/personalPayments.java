package TestNG.Parameters_WithXML;

import org.testng.annotations.Test;

public class personalPayments

{
    @Test
    public void testPersonalPayments()
    {
        System.out.println("This is testing the PERSONAL TestNG.Payments function");
    }
    @Test
    public void testPersonalPayments_1()
    {
        System.out.println("This is testing the PERSONAL Payments_1 function");
    }
    @Test
    public void testPersonalPayments_2()
    {
        System.out.println("This is testing the PERSONAL Payments_2 function");
    }
    @Test
    public void testPersonalPayments3()
    {
        System.out.println("This is testing the PERSONAL Payments3 function");
    }
}

