package TestNG.Parameters_WithXML;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class carLoans

{
    @Parameters ({"URL"})
    @Test
    public void testCarLoans(String urlName)
    {
        System.out.println("This is testing the Parameter.CAR loans function");
        System.out.println("Methods:testCarLoans The url that comes from testNG_Parameters.xml file is " + urlName);
    }

}

