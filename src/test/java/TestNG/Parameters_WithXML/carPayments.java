package TestNG.Parameters_WithXML;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class carPayments

{
    @Parameters({"January","February"})
    @Test
    public void testCarPayments(String firstMonth, String secondMonth)
    {
        System.out.println("This is testing the CAR TestNG.Payments function");
        System.out.println("The January payment is "+firstMonth+" and the February payment is "+secondMonth);

    }

}

