package TestNG.Parameters_WithXML;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class testDataParameterazation
{
    public void main(String[] args)
    {
      //  login();
    }
    @DataProvider
    public Object[][] loginCredentials()
    {
        Object [][] userNamesAndpassword = new Object[3][2];

        userNamesAndpassword[0][0] = "userName_1";
        userNamesAndpassword[0][1] = "password_1";

        userNamesAndpassword[1][0] = "userName_2";
        userNamesAndpassword[1][1] = "password_2";

        userNamesAndpassword[2][0] = "userName_3";
        userNamesAndpassword[2][1] = "password_3";

        return userNamesAndpassword;
    }


    @Test(dataProvider = "loginCredentials")
    public void login(String userName, String password)
    {
        System.out.println("enter "+userName+ " for username and "+password+" for passoword to login");
    }

//    @Test
//    public void logOut()
//    {
//        System.out.println("user logged out");
//    }
//
//    @Test
//    public void openChromeBrowser()
//    {
//        System.out.println("user open chrome.exe");
//    }
//    @Test
//    public void navigateToURL()
//    {
//        System.out.println("user navigated to abc.com url");
//    }
//
//    @Test
//    public void createCCDPayment()
//    {
//        System.out.println("user created CCD payment ");
//    }
//
//    @Test
//    public void createPPDPayment()
//    {
//        System.out.println("user created PPD payment ");
//    }
//
//    @Test
//    public void createWEBPayment()
//    {
//        System.out.println("user created WEB payment ");
//    }
//
//    @Test
//    public void createSEPAPayment()
//    {
//        System.out.println("user created SEPA payment ");
//    }

}
