package TestNG.ParallelTestRun.Loans;

import org.testng.Assert;
import org.testng.annotations.Test;

public class testBankApplication
{

   // STEP #1
    @Test
    public void openChromeBrowser()
    {
        System.out.println("user open chrome.exe");
    }
    // STEP #2
    @Test
    public void navigateToURL()
    {
        System.out.println("user navigated to abc.com url");
    }
    // STEP #3
    @Test
    public void login()
    {
        System.out.println("enter");
    }
    // STEP #4
    @Test
    public void createCCDPayment()
    {
        System.out.println("user cannot create CCD payment ");
        Assert.assertFalse(true);

    }

    // STEP #5
    @Test
    public void createPPDPayment()
    {
        System.out.println("user created PPD payment ");
    }
    // STEP #6
    @Test
    public void createWEBPayment()
    {
        System.out.println("user created WEB payment ");
    }
    // STEP #7
    @Test
    public void createSEPAPayment()
    {
        System.out.println("user created SEPA payment ");
    }
    // STEP #8
    @Test
    public void logOut()
    {
        System.out.println("user logged out");
    }


}
