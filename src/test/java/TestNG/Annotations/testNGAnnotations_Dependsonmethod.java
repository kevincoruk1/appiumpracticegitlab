package TestNG.Annotations;

import org.testng.annotations.Test;

public class testNGAnnotations_Dependsonmethod
{

    //by default the order of execution of the methods is A to Z
    @Test
    public void openChromeBrowser()
    {
        System.out.println("user open chrome.exe");
    }
    @Test
    public void navigateToURL()
    {
        System.out.println("user navigated to abc.com url");
    }
    @Test(dependsOnMethods = {"openChromeBrowser","navigateToURL"})
    public void login()
    {
        System.out.println("user entered the username and password to login");
    }
    @Test
    public void createCCDPayment()
    {
        System.out.println("user created CCD payment ");
    }

    @Test
    public void createPPDPayment()
    {
        System.out.println("user created PPD payment ");
    }

    @Test
    public void createWEBPayment()
    {
        System.out.println("user created WEB payment ");
    }

    @Test
    public void createSEPAPayment()
    {
        System.out.println("user created SEPA payment ");
    }

}
