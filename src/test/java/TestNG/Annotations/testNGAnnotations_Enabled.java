package TestNG.Annotations;

import org.testng.annotations.Test;

public class testNGAnnotations_Enabled
{
    @Test
    public void openChromeBrowser()
    {
        System.out.println("user open chrome.exe");
    }
    @Test
    public void navigateToURL()
    {
        System.out.println("user navigated to abc.com url");
    }
    @Test
    public void login()
    {
        System.out.println("user entered the username and password to login");
    }

    @Test
    public void logOut()
    {
        System.out.println("user logged out");
    }
    // assume there is a bug in the system that user can not create ccd payment.
    // using enabled=false annotation will allow us to skip this test case
    @Test(enabled = false)
    public void createCCDPayment()
    {
        System.out.println("user created CCD payment ");
    }

    @Test
    public void createPPDPayment()
    {
        System.out.println("user created PPD payment ");
    }

    @Test
    public void createWEBPayment()
    {
        System.out.println("user created WEB payment ");
    }

    @Test
    public void createSEPAPayment()
    {
        System.out.println("user created SEPA payment ");
    }

}
