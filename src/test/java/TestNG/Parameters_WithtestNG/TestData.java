package TestNG.Parameters_WithtestNG;

import org.testng.annotations.DataProvider;

public class TestData
{
    @DataProvider(name = "Inputdata")
    public Object[][] getdata()
    {

        Object[][] obj = new Object[][]
            {
                    {"Test Data#1"}, {"Test Data#2"}
            };
            return obj;
    }

}
