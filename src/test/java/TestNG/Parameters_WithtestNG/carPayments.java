package TestNG.Parameters_WithtestNG;

import org.testng.annotations.Test;

public class carPayments

{

    @Test(dataProvider = "Inputdata", dataProviderClass = TestData.class)
    public void testCarPayments(String input)
    {
        System.out.println("This method will use TestData_Parameterize class and loop through parameters in 'Inputdata' method "+input);

    }

}

