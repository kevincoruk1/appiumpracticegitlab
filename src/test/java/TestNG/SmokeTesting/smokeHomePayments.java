package TestNG.SmokeTesting;

import org.testng.annotations.Test;

public class smokeHomePayments

{
    @Test(groups = {"Smoke Testing"}) // we can use this groub flag to execute only this group in testng.xml file
    public void testHomePayments()
    {
        System.out.println("This is SMOKE testing the HOME TestNG.Payments function");
    }
}

