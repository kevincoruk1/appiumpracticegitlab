package TestNG.SmokeTesting;

import org.testng.annotations.Test;

public class smokeCarPayments

{
    @Test(groups = {"Smoke Testing"}) // we can use this groub flag to execute only this group in testng.xml file
    public void testCarPayments()
    {
        System.out.println("This is SMOKE testing the CAR TestNG.Payments function");
    }

}

